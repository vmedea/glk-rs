#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

/** Trait to be able to use strid_t::NULL to get the NULL value for the opaque handle.
 * Unfortunately, as these are typedefs for mut pointers, it is not possible to implement the
 * constants on the types directly.
 */
pub trait Null {
    const NULL: Self;
}

impl Null for winid_t { const NULL: Self = 0 as winid_t; }
impl Null for strid_t { const NULL: Self = 0 as strid_t; }
impl Null for frefid_t { const NULL: Self = 0 as frefid_t; }
impl Null for schanid_t { const NULL: Self = 0 as schanid_t; }

extern "C" {
    /** Defined in `main_wrapper.c`. */
    pub fn wrap_glk_main();
}

impl gidispatch_rock_t {
    /** Dummy rock value. */
    pub const DUMMY: Self = Self { num: 0 };
}

pub fn main() {
    unsafe {
        wrap_glk_main();
    }
}
