#ifndef GARGLK_H
#define GARGLK_H

/* garglk.h: Gargoyle Glk extensions. */

#define GLK_MODULE_GARGLKTEXT

#ifdef GLK_MODULE_GARGLKTEXT
#define gestalt_GarglkText (0x1100)

void garglk_set_zcolors(glui32 fg, glui32 bg);
void garglk_set_zcolors_stream(strid_t str, glui32 fg, glui32 bg);
void garglk_set_reversevideo(glui32 reverse);
void garglk_set_reversevideo_stream(strid_t str, glui32 reverse);

#define zcolor_Default (-1)
#define zcolor_Current (-2)
#endif

#endif
