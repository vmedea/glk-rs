/** Implements glk_exit and a wrapper around glk_main that facilitates a
 * more usable control flow using setjmp/longjmp at the C side.
 */
#include "glk.h"

#include <setjmp.h>

#ifdef _MSC_VER
/* MSVC doesn't support _Thread_local. */
#define THREAD_LOCAL __declspec(thread)
#else
#define THREAD_LOCAL _Thread_local
#endif

/** Static jump buffer.
 */
static THREAD_LOCAL jmp_buf jmpbuf;

void GLK_ATTRIBUTE_NORETURN glk_exit(void) {
    /* This function may never return. */
    longjmp(jmpbuf, 1);
}

void wrap_glk_main(void) {
    if (setjmp(jmpbuf) == 0) {
        glk_main();
    }
}
