#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
/*! Gargoyle Glk extensions. */
use crate::types::strid_t;
use crate::util::unhandled;

/** Newtype for garglk color. */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct zcolor(pub u32);
impl zcolor {
    pub const Current: Self = Self(0xfffffffe);
    pub const Default: Self = Self(0xffffffff);

    /** Color to (r,g,b), if it is not a special color. */
    pub fn to_rgb(&self) -> Option<(u8, u8, u8)> {
        if self.0 < 0x1000000 {
            Some((
                ((self.0 >> 16) & 0xff) as u8,
                ((self.0 >> 8) & 0xff) as u8,
                ((self.0 >> 0) & 0xff) as u8,
            ))
        } else {
            None
        }
    }
}

/** GLK_MODULE_GARGLKTEXT */
pub trait GarGlkText {
    /** Set current foreground and background color.
     *
     * These functions set the current text foreground and background colours. Each stream has its
     * own colours (though they will only be used for window streams). Use
     * `garglk_set_zcolors_stream` to specify a stream, or `garglk_set_zcolors` to modify the current
     * stream.
     *
     * Colour numbers are encoded RGB values in the same way as the `stylehint::TextColor` style hint.
     *
     * The negative colour numbers are special codes with the same meanings as those from the
     * Z-Machine section 8.3.7. To change only the foreground colour or only the background colour,
     * pass `zcolor::Current` for the other parameter, which will keep the current colour
     * unchanged. Pass `zcolor::Default` to reset to the current style’s foreground or
     * background colour.
     *
     * Switching to a style which has a colour stylehint set will not override the colours
     * specified with these functions. These colours will continue to be used until you disable
     * them with zcolor_Default, after which the style’s colour stylehints will be used.
     */
    fn garglk_set_zcolors(&mut self, _fg: zcolor, _bg: zcolor) {
        unhandled("garglk_set_zcolors");
    }

    /** Set current foreground and background color for stream. */
    fn garglk_set_zcolors_stream(&mut self, _str: strid_t, _fg: zcolor, _bg: zcolor) {
        unhandled("garglk_set_zcolors_stream");
    }

    /** Set reverse-video mode.
     *
     * These functions set the reverse mode. Pass a non-zero `reverse` parameter to enable reverse
     * mode, and pass 0 to disable reverse mode. Use `garglk_set_reversevideo_stream` to specify a
     * stream, or `garglk_set_reversevideo` to modify the current stream.
     *
     * Unlike colours, reverse mode stylehints do interact with these functions: reverse mode will
     * be used if either the current style has a stylehint enabling it, or if it has been enabled
     * by these functions. When stylehints enable reverse mode, you cannot call
     * `garglk_set_reversevideo` to disable it; you can only disable reverse mode by switching to
     * another style.
     */
    fn garglk_set_reversevideo(&mut self, _reverse: u32) {
        unhandled("garglk_set_reversevideo");
    }

    /** Set reverse-video mode for stream. */
    fn garglk_set_reversevideo_stream(&mut self, _str: strid_t, _reverse: u32) {
        unhandled("garglk_set_reversevideo_stream");
    }
}
