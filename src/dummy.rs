#![allow(unused_variables)]
/*! Dummy stub Glk handler.
 */
use crate::ext::{self, gluniversal_t};
use crate::traits;
use crate::types::*;
use crate::util::unhandled;
use crate::{garglk, giblorb, gidispatch};

/** Dummy Glk handler. Provides a dummy implementation for the entire Glk API,
 * logging and returning zero for every call.
 */
pub struct Dummy {}
impl traits::Base for Dummy {
    fn set_interrupt_handler(&mut self, func: Option<unsafe extern "C" fn()>) {
        unhandled("glk_set_interrupt_handler");
    }

    fn tick(&mut self) {
        unhandled("glk_tick");
    }

    fn gestalt(&mut self, sel: gestalt, val: u32, arr: &mut [u32]) -> u32 {
        unhandled("glk_gestalt");
        0
    }

    fn char_to_lower(&mut self, ch: u8) -> u8 {
        unhandled("glk_char_to_lower");
        0
    }

    fn char_to_upper(&mut self, ch: u8) -> u8 {
        unhandled("glk_char_to_upper");
        0
    }

    fn window_get_root(&mut self) -> winid_t {
        unhandled("glk_window_get_root");
        winid_t::NULL
    }

    fn window_open(
        &mut self,
        split: winid_t,
        method: winmethod,
        size: u32,
        wtype: wintype,
        rock: u32,
    ) -> winid_t {
        unhandled("glk_window_open");
        winid_t::NULL
    }

    fn window_close(&mut self, win: winid_t) -> stream_result_t {
        unhandled("glk_window_close");
        stream_result_t {
            readcount: 0,
            writecount: 0,
        }
    }

    fn window_get_size(&mut self, win: winid_t) -> (u32, u32) {
        unhandled("glk_window_get_size");
        (0, 0)
    }

    fn window_set_arrangement(
        &mut self,
        win: winid_t,
        method: winmethod,
        size: u32,
        keywin: winid_t,
    ) {
        unhandled("glk_window_get_arrangement");
    }

    fn window_get_arrangement(&mut self, win: winid_t) -> (u32, u32, winid_t) {
        unhandled("glk_window_get_arrangement");
        (0, 0, winid_t::NULL)
    }

    fn window_iterate(&mut self, win: winid_t) -> (winid_t, u32) {
        unhandled("glk_window_iterate");
        (winid_t::NULL, 0)
    }

    fn window_get_rock(&mut self, win: winid_t) -> u32 {
        unhandled("glk_window_get_rock");
        0
    }

    fn window_get_type(&mut self, win: winid_t) -> wintype {
        unhandled("glk_window_get_type");
        wintype::AllTypes
    }

    fn window_get_parent(&mut self, win: winid_t) -> winid_t {
        unhandled("glk_window_get_parent");
        winid_t::NULL
    }

    fn window_get_sibling(&mut self, win: winid_t) -> winid_t {
        unhandled("glk_window_get_sibling");
        winid_t::NULL
    }

    fn window_clear(&mut self, win: winid_t) {
        unhandled("glk_window_clear");
    }

    fn window_move_cursor(&mut self, win: winid_t, xpos: u32, ypos: u32) {
        unhandled("glk_window_move_cursor");
    }

    fn window_get_stream(&mut self, win: winid_t) -> strid_t {
        unhandled("glk_window_get_stream");
        strid_t::NULL
    }

    fn window_set_echo_stream(&mut self, win: winid_t, str: strid_t) {
        unhandled("glk_window_set_echo_stream");
    }

    fn window_get_echo_stream(&mut self, win: winid_t) -> strid_t {
        unhandled("glk_window_get_echo_stream");
        strid_t::NULL
    }

    fn set_window(&mut self, win: winid_t) {
        unhandled("glk_window_set_window");
    }

    fn stream_open_file(&mut self, fileref: frefid_t, fmode: filemode, rock: u32) -> strid_t {
        unhandled("glk_stream_open_file");
        strid_t::NULL
    }

    fn stream_open_memory(
        &mut self,
        buf: &mut gidispatch::RetainableBuffer<u8>,
        fmode: filemode,
        rock: u32,
    ) -> strid_t {
        unhandled("glk_stream_open_memory");
        strid_t::NULL
    }

    fn stream_close(&mut self, str: strid_t) -> stream_result_t {
        unhandled("glk_stream_close");
        stream_result_t {
            readcount: 0,
            writecount: 0,
        }
    }

    fn stream_iterate(&mut self, str: strid_t) -> (strid_t, u32) {
        unhandled("glk_stream_iterate");
        (strid_t::NULL, 0)
    }

    fn stream_get_rock(&mut self, str: strid_t) -> u32 {
        unhandled("glk_stream_get_rock");
        0
    }

    fn stream_set_position(&mut self, str: strid_t, pos: i32, mode: seekmode) {
        unhandled("glk_stream_set_position");
    }

    fn stream_get_position(&mut self, str: strid_t) -> u32 {
        unhandled("glk_stream_get_position");
        0
    }

    fn stream_set_current(&mut self, str: strid_t) {
        unhandled("glk_stream_set_current");
    }

    fn stream_get_current(&mut self) -> strid_t {
        unhandled("glk_stream_get_current");
        strid_t::NULL
    }

    fn put_char(&mut self, ch: u8) {
        unhandled("glk_stream_put_char");
    }

    fn put_char_stream(&mut self, str: strid_t, ch: u8) {
        unhandled("glk_stream_put_char_stream");
    }

    fn put_buffer(&mut self, buf: &[u8]) {
        unhandled("glk_put_buffer");
    }

    fn put_buffer_stream(&mut self, str: strid_t, buf: &[u8]) {
        unhandled("glk_put_buffer_stream");
    }

    fn set_style(&mut self, styl: style) {
        unhandled("glk_set_style");
    }

    fn set_style_stream(&mut self, str: strid_t, styl: style) {
        unhandled("glk_set_style_stream");
    }

    fn get_char_stream(&mut self, str: strid_t) -> i32 {
        unhandled("glk_get_char_stream");
        0
    }

    fn get_line_stream(&mut self, str: strid_t, buf: &mut [u8]) -> u32 {
        unhandled("glk_get_line_stream");
        0
    }

    fn get_buffer_stream(&mut self, str: strid_t, buf: &mut [u8]) -> u32 {
        unhandled("glk_get_buffer_stream");
        0
    }

    fn stylehint_set(&mut self, wtype: wintype, styl: style, hint: stylehint, val: i32) {
        unhandled("glk_stylehint_set");
    }

    fn stylehint_clear(&mut self, wtype: wintype, styl: style, hint: stylehint) {
        unhandled("glk_stylehint_clear");
    }

    fn style_distinguish(&mut self, win: winid_t, styl1: style, styl2: style) -> u32 {
        unhandled("glk_style_distinguish");
        0
    }

    fn style_measure(&mut self, win: winid_t, styl: style, hint: stylehint) -> Option<u32> {
        unhandled("glk_style_measure");
        None
    }

    fn fileref_create_temp(&mut self, usage: fileusage, rock: u32) -> frefid_t {
        unhandled("glk_fileref_create_temp");
        frefid_t::NULL
    }

    fn fileref_create_by_name(&mut self, usage: fileusage, name: &[u8], rock: u32) -> frefid_t {
        unhandled("glk_fileref_create_by_name");
        frefid_t::NULL
    }

    fn fileref_create_by_prompt(
        &mut self,
        usage: fileusage,
        fmode: filemode,
        rock: u32,
    ) -> frefid_t {
        unhandled("glk_fileref_create_by_prompt");
        frefid_t::NULL
    }

    fn fileref_create_from_fileref(
        &mut self,
        usage: fileusage,
        fref: frefid_t,
        rock: u32,
    ) -> frefid_t {
        unhandled("glk_fileref_create_from_fileref");
        frefid_t::NULL
    }

    fn fileref_destroy(&mut self, fref: frefid_t) {
        unhandled("glk_fileref_destroy");
    }

    fn fileref_iterate(&mut self, fref: frefid_t) -> (frefid_t, u32) {
        unhandled("glk_fileref_iterate");
        (frefid_t::NULL, 0)
    }

    fn fileref_get_rock(&mut self, fref: frefid_t) -> u32 {
        unhandled("glk_fileref_get_rock");
        0
    }

    fn fileref_delete_file(&mut self, fref: frefid_t) {
        unhandled("glk_fileref_delete_file");
    }

    fn fileref_does_file_exist(&mut self, fref: frefid_t) -> u32 {
        unhandled("glk_fileref_does_file_exist");
        0
    }

    fn select(&mut self) -> event_t {
        unhandled("glk_select");
        event_t {
            type_: 0,
            win: winid_t::NULL,
            val1: 0,
            val2: 0,
        }
    }

    fn select_poll(&mut self) -> event_t {
        unhandled("glk_select_poll");
        event_t {
            type_: 0,
            win: winid_t::NULL,
            val1: 0,
            val2: 0,
        }
    }

    fn request_timer_events(&mut self, millisecs: u32) {
        unhandled("glk_request_timer_events");
    }

    fn request_line_event(
        &mut self,
        win: winid_t,
        buf: &mut gidispatch::RetainableBuffer<u8>,
        initlen: u32,
    ) {
        unhandled("glk_request_line_event");
    }

    fn request_char_event(&mut self, win: winid_t) {
        unhandled("glk_request_char_event");
    }

    fn request_mouse_event(&mut self, win: winid_t) {
        unhandled("glk_request_mouse_event");
    }

    fn cancel_line_event(&mut self, win: winid_t) -> event_t {
        unhandled("glk_cancel_line_event");
        event_t {
            type_: 0,
            win: winid_t::NULL,
            val1: 0,
            val2: 0,
        }
    }

    fn cancel_char_event(&mut self, win: winid_t) {
        unhandled("glk_cancel_char_event");
    }

    fn cancel_mouse_event(&mut self, win: winid_t) {
        unhandled("glk_cancel_mouse_event");
    }
}
impl traits::LineEcho for Dummy {
    fn set_echo_line_event(&mut self, win: winid_t, val: u32) {
        unhandled("glk_set_echo_line_event");
    }
}
impl traits::LineTerminators for Dummy {
    fn set_terminators_line_event(&mut self, win: winid_t, keycodes: &[u32]) {
        unhandled("glk_set_terminators_line_event");
    }
}
impl traits::Unicode for Dummy {
    fn buffer_to_lower_case_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32 {
        unhandled("glk_buffer_to_lower_case_uni");
        0
    }

    fn buffer_to_upper_case_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32 {
        unhandled("glk_buffer_to_upper_case_uni");
        0
    }

    fn buffer_to_title_case_uni(&mut self, buf: &mut [u32], numchars: u32, lowerrest: u32) -> u32 {
        unhandled("glk_buffer_to_title_case_uni");
        0
    }

    fn put_char_uni(&mut self, ch: u32) {
        unhandled("glk_put_char_uni");
    }

    fn put_buffer_uni(&mut self, buf: &[u32]) {
        unhandled("glk_put_buffer_uni");
    }

    fn put_char_stream_uni(&mut self, str: strid_t, ch: u32) {
        unhandled("glk_put_char_stream_uni");
    }

    fn put_buffer_stream_uni(&mut self, str: strid_t, buf: &[u32]) {
        unhandled("glk_put_buffer_string_stream_uni");
    }

    fn get_char_stream_uni(&mut self, str: strid_t) -> i32 {
        unhandled("glk_get_char_stream_uni");
        0
    }

    fn get_buffer_stream_uni(&mut self, str: strid_t, buf: &mut [u32]) -> u32 {
        unhandled("glk_get_buffer_stream_uni");
        0
    }

    fn get_line_stream_uni(&mut self, str: strid_t, buf: &mut [u32]) -> u32 {
        unhandled("glk_get_line_stream_uni");
        0
    }

    fn stream_open_file_uni(&mut self, fileref: frefid_t, fmode: filemode, rock: u32) -> strid_t {
        unhandled("glk_stream_open_file_uni");
        strid_t::NULL
    }

    fn stream_open_memory_uni(
        &mut self,
        buf: &mut gidispatch::RetainableBuffer<u32>,
        fmode: filemode,
        rock: u32,
    ) -> strid_t {
        unhandled("glk_stream_open_memory_uni");
        strid_t::NULL
    }

    fn request_char_event_uni(&mut self, win: winid_t) {
        unhandled("glk_request_char_event_uni");
    }

    fn request_line_event_uni(
        &mut self,
        win: winid_t,
        buf: &mut gidispatch::RetainableBuffer<u32>,
        initlen: u32,
    ) {
        unhandled("glk_request_line_event_uni");
    }
}
impl traits::UnicodeNorm for Dummy {
    fn buffer_canon_decompose_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32 {
        unhandled("glk_buffer_canon_decompose_uni");
        0
    }

    fn buffer_canon_normalize_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32 {
        unhandled("glk_buffer_canon_normalize_uni");
        0
    }
}
impl traits::Image for Dummy {
    fn image_draw(&mut self, win: winid_t, image: u32, val1: i32, val2: i32) -> u32 {
        unhandled("glk_image_draw");
        0
    }

    fn image_draw_scaled(
        &mut self,
        win: winid_t,
        image: u32,
        val1: i32,
        val2: i32,
        width: u32,
        height: u32,
    ) -> u32 {
        unhandled("glk_image_draw_scaled");
        0
    }

    fn image_get_info(&mut self, image: u32) -> Option<(u32, u32)> {
        unhandled("glk_image_get_info");
        None
    }

    fn window_flow_break(&mut self, win: winid_t) {
        unhandled("glk_window_flow_break");
    }

    fn window_erase_rect(&mut self, win: winid_t, left: i32, top: i32, width: u32, height: u32) {
        unhandled("glk_window_erase_rect");
    }

    fn window_fill_rect(
        &mut self,
        win: winid_t,
        color: u32,
        left: i32,
        top: i32,
        width: u32,
        height: u32,
    ) {
        unhandled("glk_window_fill_rect");
    }

    fn window_set_background_color(&mut self, win: winid_t, color: u32) {
        unhandled("glk_window_set_background_color");
    }
}
impl traits::Sound for Dummy {
    fn schannel_create(&mut self, rock: u32) -> schanid_t {
        unhandled("glk_schannel_create");
        schanid_t::NULL
    }

    fn schannel_destroy(&mut self, chan: schanid_t) {
        unhandled("glk_schannel_destroy");
    }

    fn schannel_iterate(&mut self, chan: schanid_t) -> (schanid_t, u32) {
        unhandled("glk_schannel_iterate");
        (schanid_t::NULL, 0)
    }

    fn schannel_get_rock(&mut self, chan: schanid_t) -> u32 {
        unhandled("glk_schannel_get_rock");
        0
    }

    fn schannel_play(&mut self, chan: schanid_t, snd: u32) -> u32 {
        unhandled("glk_schannel_play");
        0
    }

    fn schannel_play_ext(&mut self, chan: schanid_t, snd: u32, repeats: u32, notify: u32) -> u32 {
        unhandled("glk_schannel_play_ext");
        0
    }

    fn schannel_stop(&mut self, chan: schanid_t) {
        unhandled("glk_schannel_stop");
    }

    fn schannel_set_volume(&mut self, chan: schanid_t, vol: u32) {
        unhandled("glk_schannel_set_volume");
    }

    fn sound_load_hint(&mut self, snd: u32, flag: u32) {
        unhandled("glk_sound_load_hint");
    }
}
impl traits::Sound2 for Dummy {
    fn schannel_create_ext(&mut self, rock: u32, volume: u32) -> schanid_t {
        unhandled("glk_schannel_create_ext");
        schanid_t::NULL
    }

    fn schannel_play_multi(
        &mut self,
        chanarray: &[schanid_t],
        sndarray: &[u32],
        notify: u32,
    ) -> u32 {
        unhandled("glk_schannel_play_multi");
        0
    }

    fn schannel_pause(&mut self, chan: schanid_t) {
        unhandled("glk_schannel_pause");
    }

    fn schannel_unpause(&mut self, chan: schanid_t) {
        unhandled("glk_schannel_unpause");
    }

    fn schannel_set_volume_ext(&mut self, chan: schanid_t, vol: u32, duration: u32, notify: u32) {
        unhandled("glk_schannel_set_volume_ext");
    }
}
impl traits::Hyperlinks for Dummy {
    fn set_hyperlink(&mut self, linkval: u32) {
        unhandled("glk_set_hyperlink");
    }

    fn set_hyperlink_stream(&mut self, str: strid_t, linkval: u32) {
        unhandled("glk_set_hyperlink_stream");
    }

    fn request_hyperlink_event(&mut self, win: winid_t) {
        unhandled("glk_request_hyperlink_event");
    }

    fn cancel_hyperlink_event(&mut self, win: winid_t) {
        unhandled("glk_cancel_hyperlink_event");
    }
}
impl traits::Datetime for Dummy {
    fn current_time(&mut self) -> glktimeval_t {
        unhandled("glk_current_time");
        glktimeval_t {
            high_sec: 0,
            low_sec: 0,
            microsec: 0,
        }
    }

    fn current_simple_time(&mut self, factor: u32) -> i32 {
        unhandled("glk_current_simple_time");
        0
    }

    fn time_to_date_utc(&mut self, time: glktimeval_t) -> glkdate_t {
        unhandled("glk_time_to_date_utc");
        glkdate_t {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: 0,
            microsec: 0,
        }
    }

    fn time_to_date_local(&mut self, time: glktimeval_t) -> glkdate_t {
        unhandled("glk_time_to_date_local");
        glkdate_t {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: 0,
            microsec: 0,
        }
    }

    fn simple_time_to_date_utc(&mut self, time: i32, factor: u32) -> glkdate_t {
        unhandled("glk_simple_time_to_date_utc");
        glkdate_t {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: 0,
            microsec: 0,
        }
    }

    fn simple_time_to_date_local(&mut self, time: i32, factor: u32) -> glkdate_t {
        unhandled("glk_simple_time_to_date_local");
        glkdate_t {
            year: 0,
            month: 0,
            day: 0,
            weekday: 0,
            hour: 0,
            minute: 0,
            second: 0,
            microsec: 0,
        }
    }

    fn date_to_time_utc(&mut self, date: glkdate_t) -> glktimeval_t {
        unhandled("glk_date_to_time_utc");
        glktimeval_t {
            high_sec: 0,
            low_sec: 0,
            microsec: 0,
        }
    }

    fn date_to_time_local(&mut self, date: glkdate_t) -> glktimeval_t {
        unhandled("glk_date_to_time_local");
        glktimeval_t {
            high_sec: 0,
            low_sec: 0,
            microsec: 0,
        }
    }

    fn date_to_simple_time_utc(&mut self, date: glkdate_t, factor: u32) -> i32 {
        unhandled("glk_date_to_simple_time_utc");
        0
    }

    fn date_to_simple_time_local(&mut self, date: glkdate_t, factor: u32) -> i32 {
        unhandled("glk_date_to_simple_time_local");
        0
    }
}
impl traits::ResourceStream for Dummy {
    fn stream_open_resource(&mut self, filenum: u32, rock: u32) -> strid_t {
        unhandled("glk_stream_open_resource");
        strid_t::NULL
    }

    fn stream_open_resource_uni(&mut self, filenum: u32, rock: u32) -> strid_t {
        unhandled("glk_stream_open_resource_uni");
        strid_t::NULL
    }
}
impl giblorb::Handlers for Dummy {
    fn giblorb_set_resource_map(&mut self, _file: strid_t) -> giblorb::giblorb_err {
        unhandled("giblorb_set_resource_map");
        giblorb::giblorb_err::None
    }

    fn giblorb_get_resource_map(&mut self) -> *mut giblorb::giblorb_map_t {
        unhandled("giblorb_get_resource_map");
        0 as *mut giblorb::giblorb_map_t
    }
}
impl gidispatch::Handlers for Dummy {
    fn gidispatch_set_object_registry(&mut self, _registry: gidispatch::ObjRegistry) {
        unhandled("gidispatch_set_object_registry");
    }

    fn gidispatch_get_objrock(
        &mut self,
        _obj: gidispatch::objref,
        _objclass: u32,
    ) -> gidispatch::rock {
        unhandled("gidispatch_get_objrock");
        gidispatch::rock::DUMMY
    }

    fn gidispatch_set_retained_registry(&mut self, _registry: gidispatch::RetainedRegistry) {
        unhandled("gidispatch_set_retained_registry");
    }

    fn gidispatch_set_autorestore_registry(
        &mut self,
        _locatearr: Option<gidispatch::AutoLocateFunc>,
        _restorearr: Option<gidispatch::AutoRestoreFunc>,
    ) {
        unhandled("gidispatch_set_autorestore_registry");
    }
}
impl garglk::GarGlkText for Dummy {
    fn garglk_set_zcolors(&mut self, _fg: garglk::zcolor, _bg: garglk::zcolor) {
        unhandled("garglk_set_zcolors");
    }

    fn garglk_set_zcolors_stream(
        &mut self,
        _str: strid_t,
        _fg: garglk::zcolor,
        _bg: garglk::zcolor,
    ) {
        unhandled("garglk_set_zcolors_stream");
    }

    fn garglk_set_reversevideo(&mut self, _reverse: u32) {
        unhandled("garglk_set_reversevideo");
    }

    fn garglk_set_reversevideo_stream(&mut self, _str: strid_t, _reverse: u32) {
        unhandled("garglk_set_reversevideo_stream");
    }
}
impl ext::Handlers for Dummy {
    fn ext_prototype(&mut self, funcnum: u32) -> Option<&'static [u8]> {
        unhandled("rustglk_ext_prototype");
        None
    }
    fn ext_call(&mut self, funcnum: u32, arglist: &mut [gluniversal_t]) {
        unhandled("rustglk_ext_call");
    }
}
impl traits::Api for Dummy {
    fn base(&mut self) -> &mut dyn traits::Base {
        self
    }
    fn line_echo(&mut self) -> Option<&mut dyn traits::LineEcho> {
        Some(self)
    }
    fn line_terminators(&mut self) -> Option<&mut dyn traits::LineTerminators> {
        Some(self)
    }
    fn unicode(&mut self) -> Option<&mut dyn traits::Unicode> {
        Some(self)
    }
    fn unicode_norm(&mut self) -> Option<&mut dyn traits::UnicodeNorm> {
        Some(self)
    }
    fn image(&mut self) -> Option<&mut dyn traits::Image> {
        Some(self)
    }
    fn sound(&mut self) -> Option<&mut dyn traits::Sound> {
        Some(self)
    }
    fn sound2(&mut self) -> Option<&mut dyn traits::Sound2> {
        Some(self)
    }
    fn hyperlinks(&mut self) -> Option<&mut dyn traits::Hyperlinks> {
        Some(self)
    }
    fn date_time(&mut self) -> Option<&mut dyn traits::Datetime> {
        Some(self)
    }
    fn resource_stream(&mut self) -> Option<&mut dyn traits::ResourceStream> {
        Some(self)
    }
    fn garglk_text(&mut self) -> Option<&mut dyn garglk::GarGlkText> {
        Some(self)
    }
    fn giblorb(&mut self) -> Option<&mut dyn giblorb::Handlers> {
        Some(self)
    }
    fn gidispatch(&mut self) -> Option<&mut dyn gidispatch::Handlers> {
        Some(self)
    }
    fn ext(&mut self) -> Option<&mut dyn ext::Handlers> {
        Some(self)
    }
}
