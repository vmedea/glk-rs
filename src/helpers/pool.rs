//! A simple object pool.
//! Based on obj-pool 0.4.3 by Artem Shein, Stjepan Glavina.
use std::hint::unreachable_unchecked;
use std::marker::PhantomData;
use std::slice;
use std::{
    fmt, iter, mem,
    ops::{Index, IndexMut},
    ptr, vec,
};

/// "Null" valud for indices. This is always an invalid index.
pub const NULL_INDEX: usize = std::usize::MAX;

/// Trait for opaque object IDs that can be mapped from and to a pool index.
/// There is also a special NULL value, which indicates the absence of an index.
pub trait ObjId: Copy + PartialEq {
    const NULL: Self;
    /// Convert an usize to an object id.
    /// This will never be passed an invalid index.
    fn from_index(val: usize) -> Self;
    /// Convert the object id to an index.
    /// Must return NULL_INDEX for Self::NULL.
    fn into_index(self) -> usize;
}

/// A slot, which is either vacant or occupied.
///
/// Vacant slots in object pool are linked together into a singly linked list. This allows the object pool to
/// efficiently find a vacant slot before inserting a new object, or reclaiming a slot after
/// removing an object.
#[derive(Clone)]
enum Slot<T> {
    /// Vacant slot, containing index to the next slot in the linked list.
    Vacant(usize),

    /// Occupied slot, containing a value.
    Occupied(T),
}

/// An object pool.
///
/// `ObjPool<T>` holds an array of slots for storing objects.
/// Every slot is always in one of two states: occupied or vacant.
///
/// Essentially, this is equivalent to `Vec<Option<T>>`.
///
/// It is used for mapping opaque handles (implementing the ObjId trait) to objects without
/// the overhead of a hash map, or the unsafety of using bare pointers.
///
/// # Insert and remove
///
/// When inserting a new object into object pool, a vacant slot is found and then the object is placed
/// into the slot. If there are no vacant slots, the array is reallocated with bigger capacity.
/// The cost of insertion is amortized `O(1)`.
///
/// When removing an object, the slot containing it is marked as vacant and the object is returned.
/// The cost of removal is `O(1)`.
///
/// ```
/// use obj_pool::ObjPool;
///
/// let mut obj_pool = ObjPool::new();
/// let a = obj_pool.insert(10);
/// let b = obj_pool.insert(20);
///
/// assert_ne!(a, b); // ids are not the same
///
/// assert_eq!(obj_pool.remove(a), Some(10));
/// assert_eq!(obj_pool.get(a), None); // there is no object with this `ObjId` anymore
///
/// assert_eq!(obj_pool.insert(30), a); // slot is reused, got the same `ObjId`
/// ```
///
/// # Indexing
///
/// You can also access objects in an object pool by `ObjId`.
/// However, accessing an object with invalid `ObjId` will result in panic.
///
/// ```
/// use obj_pool::ObjPool;
///
/// let mut obj_pool = ObjPool::new();
/// let a = obj_pool.insert(10);
/// let b = obj_pool.insert(20);
///
/// assert_eq!(obj_pool[a], 10);
/// assert_eq!(obj_pool[b], 20);
///
/// obj_pool[a] += obj_pool[b];
/// assert_eq!(obj_pool[a], 30);
/// ```
///
/// To access slots without fear of panicking, use `get` and `get_mut`, which return `Option`s.
pub struct ObjPool<T, O> {
    /// Slots in which objects are stored.
    slots: Vec<Slot<T>>,

    /// Number of occupied slots in the object pool.
    len: usize,

    /// Index of the first vacant slot in the linked list.
    head: usize,

    phantom: PhantomData<O>,
}

impl<T, O> AsRef<ObjPool<T, O>> for ObjPool<T, O> {
    fn as_ref(&self) -> &ObjPool<T, O> {
        self
    }
}

impl<T, O> AsMut<ObjPool<T, O>> for ObjPool<T, O> {
    fn as_mut(&mut self) -> &mut ObjPool<T, O> {
        self
    }
}

impl<T, O> ObjPool<T, O>
where
    O: ObjId,
{
    /// Constructs a new, empty object pool.
    ///
    /// The object pool will not allocate until objects are inserted into it.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool: ObjPool<i32> = ObjPool::new();
    /// ```
    #[inline]
    pub fn new() -> Self {
        ObjPool {
            slots: Vec::new(),
            len: 0,
            head: NULL_INDEX,
            phantom: PhantomData,
        }
    }

    /// Constructs a new, empty object pool with the specified capacity (number of slots).
    ///
    /// The object pool will be able to hold exactly `capacity` objects without reallocating.
    /// If `capacity` is 0, the object pool will not allocate.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::with_capacity(10);
    ///
    /// assert_eq!(obj_pool.len(), 0);
    /// assert_eq!(obj_pool.capacity(), 10);
    ///
    /// // These inserts are done without reallocating...
    /// for i in 0..10 {
    ///     obj_pool.insert(i);
    /// }
    /// assert_eq!(obj_pool.capacity(), 10);
    ///
    /// // ... but this one will reallocate.
    /// obj_pool.insert(11);
    /// assert!(obj_pool.capacity() > 10);
    /// ```
    #[inline]
    pub fn with_capacity(cap: usize) -> Self {
        ObjPool {
            slots: Vec::with_capacity(cap),
            len: 0,
            head: NULL_INDEX,
            phantom: PhantomData,
        }
    }

    /// Returns the number of slots in the object pool.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let obj_pool: ObjPool<i32> = ObjPool::with_capacity(10);
    /// assert_eq!(obj_pool.capacity(), 10);
    /// ```
    #[inline]
    pub fn capacity(&self) -> usize {
        self.slots.capacity()
    }

    /// Returns the number of occupied slots in the object pool.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// assert_eq!(obj_pool.len(), 0);
    ///
    /// for i in 0..10 {
    ///     obj_pool.insert(());
    ///     assert_eq!(obj_pool.len(), i + 1);
    /// }
    /// ```
    #[inline]
    pub fn len(&self) -> usize {
        self.len
    }

    /// Returns `true` if all slots are vacant.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// assert!(obj_pool.is_empty());
    ///
    /// obj_pool.insert(1);
    /// assert!(!obj_pool.is_empty());
    /// ```
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    /// Returns the `ObjId` of the next inserted object if no other
    /// mutating calls take place in between.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    ///
    /// let a = obj_pool.next_vacant();
    /// let b = obj_pool.insert(1);
    /// assert_eq!(a, b);
    /// let c = obj_pool.next_vacant();
    /// let d = obj_pool.insert(2);
    /// assert_eq!(c, d);
    /// ```
    #[inline]
    pub fn next_vacant(&mut self) -> O {
        if self.head == NULL_INDEX {
            O::from_index(self.len)
        } else {
            O::from_index(self.head)
        }
    }

    /// Inserts an object into the object pool and returns the `ObjId` of this object,
    /// as well as a mutable reference to the newly inserted object.
    /// The object pool will reallocate if it's full.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    ///
    /// let a = obj_pool.insert(1);
    /// let b = obj_pool.insert(2);
    /// assert!(a != b);
    /// ```
    pub fn insert(&mut self, object: T) -> (O, &mut T) {
        self.len += 1;

        let index = if self.head == NULL_INDEX {
            self.slots.push(Slot::Occupied(object));
            self.len - 1
        } else {
            let index = self.head;
            match self.slots[index] {
                Slot::Vacant(next) => {
                    self.head = next;
                    self.slots[index] = Slot::Occupied(object);
                }
                Slot::Occupied(_) => unreachable!(),
            }
            index
        };
        // We know for sure that the index returned above is a valid index, and
        // points to an Occupied slot.
        let vref = unsafe {
            match self.slots.get_unchecked_mut(index) {
                &mut Slot::Vacant(_) => unreachable_unchecked(),
                &mut Slot::Occupied(ref mut object) => object,
            }
        };
        (O::from_index(index), vref)
    }

    /// Removes the object stored by `ObjId` from the object pool and returns it.
    ///
    /// `None` is returned in case the there is no object with such an `ObjId`.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// let a = obj_pool.insert("hello");
    ///
    /// assert_eq!(obj_pool.len(), 1);
    /// assert_eq!(obj_pool.remove(a), Some("hello"));
    ///
    /// assert_eq!(obj_pool.len(), 0);
    /// assert_eq!(obj_pool.remove(a), None);
    /// ```
    pub fn remove(&mut self, obj_id: O) -> Option<T> {
        let index = obj_id.into_index();
        match self.slots.get_mut(index) {
            None => None,
            Some(&mut Slot::Vacant(_)) => None,
            Some(slot @ &mut Slot::Occupied(_)) => {
                if let Slot::Occupied(object) = mem::replace(slot, Slot::Vacant(self.head)) {
                    self.head = index;
                    self.len -= 1;
                    Some(object)
                } else {
                    unreachable!();
                }
            }
        }
    }

    /// Clears the object pool, removing and dropping all objects it holds. Keeps the allocated memory
    /// for reuse.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// for i in 0..10 {
    ///     obj_pool.insert(i);
    /// }
    ///
    /// assert_eq!(obj_pool.len(), 10);
    /// obj_pool.clear();
    /// assert_eq!(obj_pool.len(), 0);
    /// ```
    #[inline]
    pub fn clear(&mut self) {
        self.slots.clear();
        self.len = 0;
        self.head = NULL_INDEX;
    }

    /// Returns a reference to the object by its `ObjId`.
    ///
    /// If object is not found with given `obj_id`, `None` is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// let obj_id = obj_pool.insert("hello");
    ///
    /// assert_eq!(obj_pool.get(obj_id), Some(&"hello"));
    /// obj_pool.remove(obj_id);
    /// assert_eq!(obj_pool.get(obj_id), None);
    /// ```
    pub fn get(&self, obj_id: O) -> Option<&T> {
        let index = obj_id.into_index();
        match self.slots.get(index) {
            None => None,
            Some(&Slot::Vacant(_)) => None,
            Some(&Slot::Occupied(ref object)) => Some(object),
        }
    }

    /// Returns a mutable reference to the object by its `ObjId`.
    ///
    /// If object can't be found, `None` is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// let obj_id = obj_pool.insert(7);
    ///
    /// assert_eq!(obj_pool.get_mut(obj_id), Some(&mut 7));
    /// *obj_pool.get_mut(obj_id).unwrap() *= 10;
    /// assert_eq!(obj_pool.get_mut(obj_id), Some(&mut 70));
    /// ```
    #[inline]
    pub fn get_mut(&mut self, obj_id: O) -> Option<&mut T> {
        let index = obj_id.into_index();
        match self.slots.get_mut(index) {
            None => None,
            Some(&mut Slot::Vacant(_)) => None,
            Some(&mut Slot::Occupied(ref mut object)) => Some(object),
        }
    }

    /// Returns a reference to the object by its `ObjId`.
    ///
    /// # Safety
    ///
    /// Behavior is undefined if object can't be found.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// let obj_id = obj_pool.insert("hello");
    ///
    /// unsafe { assert_eq!(&*obj_pool.get_unchecked(obj_id), &"hello") }
    /// ```
    pub unsafe fn get_unchecked(&self, obj_id: O) -> &T {
        match self.slots.get(obj_id.into_index()) {
            None => unreachable_unchecked(),
            Some(&Slot::Vacant(_)) => unreachable_unchecked(),
            Some(&Slot::Occupied(ref object)) => object,
        }
    }

    /// Returns a mutable reference to the object by its `ObjId`.
    ///
    /// # Safety
    ///
    /// Behavior is undefined if object can't be found.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// let obj_id = obj_pool.insert("hello");
    ///
    /// unsafe { assert_eq!(&*obj_pool.get_unchecked_mut(obj_id), &"hello") }
    /// ```
    pub unsafe fn get_unchecked_mut(&mut self, obj_id: O) -> &mut T {
        let index = obj_id.into_index();
        match self.slots.get_mut(index) {
            None => unreachable_unchecked(),
            Some(&mut Slot::Vacant(_)) => unreachable_unchecked(),
            Some(&mut Slot::Occupied(ref mut object)) => object,
        }
    }

    /// Swaps two objects in the object pool.
    ///
    /// The two `ObjId`s are `a` and `b`.
    ///
    /// # Panics
    ///
    /// Panics if any of the `ObjId`s is invalid.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// let a = obj_pool.insert(7);
    /// let b = obj_pool.insert(8);
    ///
    /// obj_pool.swap(a, b);
    /// assert_eq!(obj_pool.get(a), Some(&8));
    /// assert_eq!(obj_pool.get(b), Some(&7));
    /// ```
    #[inline]
    pub fn swap(&mut self, a: O, b: O) {
        unsafe {
            let fst = self.get_mut(a).unwrap() as *mut _;
            let snd = self.get_mut(b).unwrap() as *mut _;
            if a != b {
                ptr::swap(fst, snd);
            }
        }
    }

    /// Reserves capacity for at least `additional` more objects to be inserted. The object pool may
    /// reserve more space to avoid frequent reallocations.
    ///
    /// # Panics
    ///
    /// Panics if the new capacity overflows `usize`.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// obj_pool.insert("hello");
    ///
    /// obj_pool.reserve(10);
    /// assert!(obj_pool.capacity() >= 11);
    /// ```
    pub fn reserve(&mut self, additional: usize) {
        let vacant = self.slots.len() - self.len;
        if additional > vacant {
            self.slots.reserve(additional - vacant);
        }
    }

    /// Reserves the minimum capacity for exactly `additional` more objects to be inserted.
    ///
    /// Note that the allocator may give the object pool more space than it requests.
    ///
    /// # Panics
    ///
    /// Panics if the new capacity overflows `usize`.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// obj_pool.insert("hello");
    ///
    /// obj_pool.reserve_exact(10);
    /// assert!(obj_pool.capacity() >= 11);
    /// ```
    pub fn reserve_exact(&mut self, additional: usize) {
        let vacant = self.slots.len() - self.len;
        if additional > vacant {
            self.slots.reserve_exact(additional - vacant);
        }
    }

    /// Returns an iterator over occupied slots.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// obj_pool.insert(1);
    /// obj_pool.insert(2);
    /// obj_pool.insert(4);
    ///
    /// let mut iterator = obj_pool.iter();
    /// assert_eq!(iterator.next(), Some((obj_pool.index_to_obj_id(0), &1)));
    /// assert_eq!(iterator.next(), Some((obj_pool.index_to_obj_id(1), &2)));
    /// assert_eq!(iterator.next(), Some((obj_pool.index_to_obj_id(2), &4)));
    /// ```
    #[inline]
    pub fn iter(&self) -> Iter<T, O> {
        Iter {
            slots: self.slots.iter().enumerate(),
            phantom: PhantomData,
        }
    }

    /// Returns an iterator that returns mutable references to objects.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::new();
    /// obj_pool.insert("zero".to_string());
    /// obj_pool.insert("one".to_string());
    /// obj_pool.insert("two".to_string());
    ///
    /// let offset = obj_pool.offset();
    /// for (obj_id, object) in obj_pool.iter_mut() {
    ///     *object = obj_id.into_index(offset).to_string() + " " + object;
    /// }
    ///
    /// let mut iterator = obj_pool.iter();
    /// assert_eq!(iterator.next(), Some((obj_pool.index_to_obj_id(0), &"0 zero".to_string())));
    /// assert_eq!(iterator.next(), Some((obj_pool.index_to_obj_id(1), &"1 one".to_string())));
    /// assert_eq!(iterator.next(), Some((obj_pool.index_to_obj_id(2), &"2 two".to_string())));
    /// ```
    #[inline]
    pub fn iter_mut(&mut self) -> IterMut<T, O> {
        IterMut {
            slots: self.slots.iter_mut().enumerate(),
            phantom: PhantomData,
        }
    }

    /// Shrinks the capacity of the object pool as much as possible.
    ///
    /// It will drop down as close as possible to the length but the allocator may still inform
    /// the object pool that there is space for a few more elements.
    ///
    /// # Examples
    ///
    /// ```
    /// use obj_pool::ObjPool;
    ///
    /// let mut obj_pool = ObjPool::with_capacity(10);
    /// obj_pool.insert("first".to_string());
    /// obj_pool.insert("second".to_string());
    /// obj_pool.insert("third".to_string());
    /// assert_eq!(obj_pool.capacity(), 10);
    /// obj_pool.shrink_to_fit();
    /// assert!(obj_pool.capacity() >= 3);
    /// ```
    pub fn shrink_to_fit(&mut self) {
        self.slots.shrink_to_fit();
    }

    /// Find next object starting from obj_id O, or O::NULL if at the end.
    /// If obj_id is O::NULL, start from the beginning.
    #[inline]
    pub fn next(&mut self, obj_id: O) -> O {
        let mut index = obj_id.into_index().wrapping_add(1);
        while index < self.slots.len() {
            match self.slots[index] {
                Slot::Occupied(_) => {
                    return O::from_index(index);
                }
                _ => {}
            }
            index += 1;
        }
        O::NULL
    }
}

impl<T, O> fmt::Debug for ObjPool<T, O> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ObjPool {{ ... }}")
    }
}

impl<T, O> Index<O> for ObjPool<T, O>
where
    O: ObjId,
{
    type Output = T;

    #[inline]
    fn index(&self, obj_id: O) -> &T {
        self.get(obj_id).expect("object not found")
    }
}

impl<T, O> IndexMut<O> for ObjPool<T, O>
where
    O: ObjId,
{
    #[inline]
    fn index_mut(&mut self, obj_id: O) -> &mut T {
        self.get_mut(obj_id).expect("object not found")
    }
}

impl<T, O> Default for ObjPool<T, O>
where
    O: ObjId,
{
    fn default() -> Self {
        ObjPool::new()
    }
}

impl<T: Clone, O> Clone for ObjPool<T, O> {
    fn clone(&self) -> Self {
        ObjPool {
            slots: self.slots.clone(),
            len: self.len,
            head: self.head,
            phantom: PhantomData,
        }
    }
}

/// An iterator over the occupied slots in a `ObjPool`.
pub struct IntoIter<T, O> {
    slots: iter::Enumerate<vec::IntoIter<Slot<T>>>,
    phantom: PhantomData<O>,
}

impl<T, O> Iterator for IntoIter<T, O>
where
    O: ObjId,
{
    type Item = (O, T);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, slot)) = self.slots.next() {
            if let Slot::Occupied(object) = slot {
                return Some((O::from_index(index), object));
            }
        }
        None
    }
}

impl<T, O> IntoIterator for ObjPool<T, O>
where
    O: ObjId,
{
    type Item = (O, T);
    type IntoIter = IntoIter<T, O>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        IntoIter {
            slots: self.slots.into_iter().enumerate(),
            phantom: PhantomData,
        }
    }
}

impl<T, O> iter::FromIterator<T> for ObjPool<T, O>
where
    O: ObjId,
{
    fn from_iter<U: IntoIterator<Item = T>>(iter: U) -> ObjPool<T, O> {
        let iter = iter.into_iter();
        let mut obj_pool = ObjPool::with_capacity(iter.size_hint().0);
        for i in iter {
            obj_pool.insert(i);
        }
        obj_pool
    }
}

impl<T, O> fmt::Debug for IntoIter<T, O> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "IntoIter {{ ... }}")
    }
}

/// An iterator over references to the occupied slots in a `ObjPool`.
pub struct Iter<'a, T: 'a, O> {
    slots: iter::Enumerate<slice::Iter<'a, Slot<T>>>,
    phantom: PhantomData<O>,
}

impl<'a, T, O> Iterator for Iter<'a, T, O>
where
    O: ObjId,
{
    type Item = (O, &'a T);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, slot)) = self.slots.next() {
            if let Slot::Occupied(ref object) = *slot {
                return Some((O::from_index(index), object));
            }
        }
        None
    }
}

impl<'a, T, O> IntoIterator for &'a ObjPool<T, O>
where
    O: ObjId,
{
    type Item = (O, &'a T);
    type IntoIter = Iter<'a, T, O>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T, O> fmt::Debug for Iter<'a, T, O> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Iter {{ ... }}")
    }
}

/// An iterator over mutable references to the occupied slots in a `ObjPool`.
pub struct IterMut<'a, T: 'a, O> {
    slots: iter::Enumerate<slice::IterMut<'a, Slot<T>>>,
    phantom: PhantomData<O>,
}

impl<'a, T, O> Iterator for IterMut<'a, T, O>
where
    O: ObjId,
{
    type Item = (O, &'a mut T);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, slot)) = self.slots.next() {
            if let Slot::Occupied(ref mut object) = *slot {
                return Some((O::from_index(index), object));
            }
        }
        None
    }
}

impl<'a, T, O> IntoIterator for &'a mut ObjPool<T, O>
where
    O: ObjId,
{
    type Item = (O, &'a mut T);
    type IntoIter = IterMut<'a, T, O>;

    #[inline]
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<'a, T, O> fmt::Debug for IterMut<'a, T, O> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "IterMut {{ ... }}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl ObjId for usize {
        const NULL: usize = NULL_INDEX;
        fn from_index(val: usize) -> Self {
            val
        }
        fn into_index(self) -> usize {
            self
        }
    }

    #[test]
    fn new() {
        let obj_pool = ObjPool::<i32, usize>::new();
        assert!(obj_pool.is_empty());
        assert_eq!(obj_pool.len(), 0);
        assert_eq!(obj_pool.capacity(), 0);
    }

    #[test]
    fn insert() {
        let mut obj_pool = ObjPool::<_, usize>::new();

        for i in 0..10 {
            let a = obj_pool.insert(i * 10).0;
            assert_eq!(obj_pool[a], i * 10);
        }
        assert!(!obj_pool.is_empty());
        assert_eq!(obj_pool.len(), 10);
    }

    #[test]
    fn with_capacity() {
        let mut obj_pool = ObjPool::<_, usize>::with_capacity(10);
        assert_eq!(obj_pool.capacity(), 10);

        for _ in 0..10 {
            obj_pool.insert(());
        }
        assert_eq!(obj_pool.len(), 10);
        assert_eq!(obj_pool.capacity(), 10);

        obj_pool.insert(());
        assert_eq!(obj_pool.len(), 11);
        assert!(obj_pool.capacity() > 10);
    }

    #[test]
    fn remove() {
        let mut obj_pool = ObjPool::<_, usize>::new();

        let a = obj_pool.insert(0).0;
        let b = obj_pool.insert(10).0;
        let c = obj_pool.insert(20).0;
        obj_pool.insert(30);
        assert_eq!(obj_pool.len(), 4);

        assert_eq!(obj_pool.remove(b), Some(10));
        assert_eq!(obj_pool.remove(c), Some(20));
        assert_eq!(obj_pool.len(), 2);

        obj_pool.insert(-1);
        obj_pool.insert(-1);
        assert_eq!(obj_pool.len(), 4);

        assert_eq!(obj_pool.remove(a), Some(0));
        obj_pool.insert(-1);
        assert_eq!(obj_pool.len(), 4);

        obj_pool.insert(400);
        assert_eq!(obj_pool.len(), 5);
    }

    #[test]
    fn clear() {
        let mut obj_pool = ObjPool::<_, usize>::new();
        obj_pool.insert(10);
        obj_pool.insert(20);

        assert!(!obj_pool.is_empty());
        assert_eq!(obj_pool.len(), 2);

        let cap = obj_pool.capacity();
        obj_pool.clear();

        assert!(obj_pool.is_empty());
        assert_eq!(obj_pool.len(), 0);
        assert_eq!(obj_pool.capacity(), cap);
    }

    #[test]
    fn indexing() {
        let mut obj_pool = ObjPool::<_, usize>::new();

        let a = obj_pool.insert(10).0;
        let b = obj_pool.insert(20).0;
        let c = obj_pool.insert(30).0;

        obj_pool[b] += obj_pool[c];
        assert_eq!(obj_pool[a], 10);
        assert_eq!(obj_pool[b], 50);
        assert_eq!(obj_pool[c], 30);
    }

    #[test]
    #[should_panic]
    fn indexing_vacant() {
        let mut obj_pool = ObjPool::<_, usize>::new();

        let _ = obj_pool.insert(10).0;
        let b = obj_pool.insert(20).0;
        let _ = obj_pool.insert(30).0;

        obj_pool.remove(b);
        obj_pool[b];
    }

    #[test]
    #[should_panic]
    fn invalid_indexing() {
        let mut obj_pool = ObjPool::<_, usize>::new();

        obj_pool.insert(10);
        obj_pool.insert(20);
        let a = obj_pool.insert(30).0;
        obj_pool.remove(a);

        obj_pool[a];
    }

    #[test]
    fn get() {
        let mut obj_pool = ObjPool::<_, usize>::new();

        let a = obj_pool.insert(10).0;
        let b = obj_pool.insert(20).0;
        let c = obj_pool.insert(30).0;

        *obj_pool.get_mut(b).unwrap() += *obj_pool.get(c).unwrap();
        assert_eq!(obj_pool.get(a), Some(&10));
        assert_eq!(obj_pool.get(b), Some(&50));
        assert_eq!(obj_pool.get(c), Some(&30));

        obj_pool.remove(b);
        assert_eq!(obj_pool.get(b), None);
        assert_eq!(obj_pool.get_mut(b), None);
    }

    #[test]
    fn reserve() {
        let mut obj_pool = ObjPool::<_, usize>::new();
        obj_pool.insert(1);
        obj_pool.insert(2);

        obj_pool.reserve(10);
        assert!(obj_pool.capacity() >= 11);
    }

    #[test]
    fn reserve_exact() {
        let mut obj_pool = ObjPool::<_, usize>::new();
        obj_pool.insert(1);
        obj_pool.insert(2);
        obj_pool.reserve(10);
        assert!(obj_pool.capacity() >= 11);
    }

    #[test]
    fn iter() {
        let mut arena = ObjPool::<_, usize>::new();
        let a = arena.insert(10).0;
        let b = arena.insert(20).0;
        let c = arena.insert(30).0;
        let d = arena.insert(40).0;

        arena.remove(b);

        let mut it = arena.iter();
        assert_eq!(it.next(), Some((a, &10)));
        assert_eq!(it.next(), Some((c, &30)));
        assert_eq!(it.next(), Some((d, &40)));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn iter_mut() {
        let mut obj_pool = ObjPool::<_, usize>::new();
        let a = obj_pool.insert(10).0;
        let b = obj_pool.insert(20).0;
        let c = obj_pool.insert(30).0;
        let d = obj_pool.insert(40).0;

        obj_pool.remove(b);

        {
            let mut it = obj_pool.iter_mut();
            assert_eq!(it.next(), Some((a, &mut 10)));
            assert_eq!(it.next(), Some((c, &mut 30)));
            assert_eq!(it.next(), Some((d, &mut 40)));
            assert_eq!(it.next(), None);
        }

        for (obj_id, value) in &mut obj_pool {
            *value += obj_id;
        }

        let mut it = obj_pool.iter_mut();
        assert_eq!(*it.next().unwrap().1, 10 + a);
        assert_eq!(*it.next().unwrap().1, 30 + c);
        assert_eq!(*it.next().unwrap().1, 40 + d);
        assert_eq!(it.next(), None);
    }

    #[test]
    fn from_iter() {
        let obj_pool: ObjPool<_, usize> = [10, 20, 30, 40].iter().copied().collect();

        let mut it = obj_pool.iter();
        assert_eq!(it.next(), Some((usize::from_index(0), &10)));
        assert_eq!(it.next(), Some((usize::from_index(1), &20)));
        assert_eq!(it.next(), Some((usize::from_index(2), &30)));
        assert_eq!(it.next(), Some((usize::from_index(3), &40)));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn next() {
        let mut arena = ObjPool::<_, usize>::new();

        assert_eq!(arena.next(usize::NULL), usize::NULL);

        let a = arena.insert(10).0;
        let b = arena.insert(20).0;
        let c = arena.insert(30).0;
        let d = arena.insert(40).0;

        arena.remove(b);

        assert_eq!(arena.next(usize::NULL), a);
        assert_eq!(arena.next(a), c);
        assert_eq!(arena.next(c), d);
        assert_eq!(arena.next(d), usize::NULL);

        arena.remove(a);

        assert_eq!(arena.next(usize::NULL), c);
        assert_eq!(arena.next(c), d);
        assert_eq!(arena.next(d), usize::NULL);

        arena.remove(c);
        arena.remove(d);

        assert_eq!(arena.next(usize::NULL), usize::NULL);
    }
}
