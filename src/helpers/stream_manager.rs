use std::cell::RefCell;
use std::convert::TryInto;
use std::fs::{File, OpenOptions};
use std::path::Path;
use std::rc::Rc;

use crate::gidispatch;
use crate::helpers::file_stream::{FileStreamBin32, FileStreamBin8, FileStreamUTF8};
use crate::helpers::memory_stream::{MemoryStream32, MemoryStream8};
use crate::helpers::pool;
use crate::helpers::stream::{Stream, Style};
use crate::helpers::window_stream::{WindowStream, WindowStreamFuncs};
use crate::types::{filemode, fileusage, seekmode, stream_result_t, strid_t, Null};
use crate::util::unhandled;

/** Handles stream-related Glk calls. */
pub struct StreamManager {
    /** Object registry callbacks. */
    obj_registry: gidispatch::ObjRegistry,
    /** Retained registry callbacks. */
    retained_registry: gidispatch::RetainedRegistry,
    /** Current stream. */
    current: strid_t,
    /** Pool of streams. */
    streams: pool::ObjPool<Box<dyn Stream>, strid_t>,
}

impl StreamManager {
    pub fn new() -> Self {
        Self {
            obj_registry: gidispatch::ObjRegistry::new(),
            retained_registry: gidispatch::RetainedRegistry::new(),
            current: strid_t::NULL,
            streams: pool::ObjPool::new(),
        }
    }

    pub fn set_object_registry(&mut self, obj_registry: gidispatch::ObjRegistry) {
        self.obj_registry = obj_registry;
        // Register existing streams
        for (id, obj) in self.streams.iter_mut() {
            obj.set_di_rock(obj_registry.register(id));
        }
    }

    pub fn set_retained_registry(&mut self, retained_registry: gidispatch::RetainedRegistry) {
        self.retained_registry = retained_registry;
    }

    /** Make stream from open file. */
    pub fn from_file(&mut self, gamefile: File) -> strid_t {
        self.streams
            .insert(Box::new(FileStreamBin8::new(0, gamefile)))
            .0
    }

    /** Make stream from file to open. */
    pub fn open_file(
        &mut self,
        usage: fileusage,
        path: &Path,
        fmode: filemode,
        rock: u32,
        unicode: bool,
    ) -> strid_t {
        // "If fmode is filemode_Read, the file must already exist; for the other modes, an empty file is created if none exists."
        // "If fmode is filemode_Write, and the file already exists, it is truncated
        // down to zero length (an empty file); the other modes do not truncate."
        let mut options = OpenOptions::new();
        match fmode {
            filemode::Write => options.write(true).create(true).truncate(true),
            filemode::Read => options.read(true),
            filemode::ReadWrite => options.read(true).write(true).create(true),
            filemode::WriteAppend => options.write(true).append(true).create(true),
            _ => {
                unhandled("glk_stream_open_file [unknown filemode]");
                return strid_t::NULL;
            }
        };
        if let Ok(file) = options.open(path) {
            let (id, stream) = self.streams.insert(match (usage.is_text(), unicode) {
                (false, false) => Box::new(FileStreamBin8::new(rock, file)),
                (false, true) => Box::new(FileStreamBin32::new(rock, file)),
                // There is no special filestream for non-unicode text, because we assume "platform
                // native text" is UTF-8.
                (true, false) => Box::new(FileStreamUTF8::new(rock, file)),
                (true, true) => Box::new(FileStreamUTF8::new(rock, file)),
            });
            stream.set_di_rock(self.obj_registry.register(id));
            id
        } else {
            unhandled("glk_stream_open_file [could not open file]");
            strid_t::NULL
        }
    }

    pub fn open_memory(
        &mut self,
        buf: &mut gidispatch::RetainableBuffer<u8>,
        fmode: filemode,
        rock: u32,
    ) -> strid_t {
        let buf = self.retained_registry.register(buf);
        let (id, stream) = self
            .streams
            .insert(Box::new(MemoryStream8::new(rock, buf, fmode)));
        stream.set_di_rock(self.obj_registry.register(id));
        id
    }

    pub fn open_memory_uni(
        &mut self,
        buf: &mut gidispatch::RetainableBuffer<u32>,
        fmode: filemode,
        rock: u32,
    ) -> strid_t {
        let buf = self.retained_registry.register(buf);
        let (ret, stream) = self
            .streams
            .insert(Box::new(MemoryStream32::new(rock, buf, fmode)));
        stream.set_di_rock(self.obj_registry.register(ret));
        ret
    }

    /** Internal-only method to open a window stream. */
    pub fn open_window(&mut self, window: Rc<RefCell<dyn WindowStreamFuncs>>) -> strid_t {
        let window = Rc::downgrade(&window);
        let (ret, stream) = self.streams.insert(Box::new(WindowStream::new(0, window)));
        stream.set_di_rock(self.obj_registry.register(ret));
        ret
    }

    /** Internal-only method to remove a stream. */
    pub fn remove(&mut self, str: strid_t) {
        let stream = self.streams.get_mut(str).unwrap();
        // TODO: remove as echo stream for any windows that have it as echo stream
        // If current stream, set current stream to 0
        if str == self.current {
            self.current = strid_t::NULL;
        }
        // Unregister stream from object registry
        self.obj_registry.unregister(str, stream.get_di_rock());
        // Finally, really remove it
        self.streams.remove(str);
    }

    pub fn close(&mut self, str: strid_t) -> stream_result_t {
        // note: "You cannot close window streams; use glk_window_close() instead."
        let stream = self.streams.get_mut(str).unwrap();
        if let Some(result) = stream.close() {
            self.remove(str);
            result
        } else {
            // Cannot close stream (e.g. is a window stream)
            stream_result_t {
                readcount: 0,
                writecount: 0,
            }
        }
    }

    pub fn iterate(&mut self, str: strid_t) -> (strid_t, u32) {
        let str = self.streams.next(str);
        if let Some(stream) = self.streams.get(str) {
            (str, stream.get_rock())
        } else {
            (str, 0)
        }
    }

    pub fn get_rock(&mut self, str: strid_t) -> u32 {
        self.streams.get(str).unwrap().get_rock()
    }

    pub fn set_position(&mut self, stream: strid_t, pos: i32, mode: seekmode) {
        self.streams
            .get_mut(stream)
            .unwrap()
            .set_position(pos, mode)
    }

    pub fn get_position(&mut self, stream: strid_t) -> u32 {
        self.streams.get_mut(stream).unwrap().get_position()
    }

    pub fn set_current(&mut self, str: strid_t) {
        self.current = str;
    }

    pub fn get_current(&mut self) -> strid_t {
        self.current
    }

    pub fn put_char(&mut self, ch: u8) {
        self.put_buffer_stream(self.current, &[ch])
    }

    pub fn put_char_stream(&mut self, str: strid_t, ch: u8) {
        self.put_buffer_stream(str, &[ch])
    }

    pub fn put_buffer(&mut self, buf: &[u8]) {
        self.put_buffer_stream(self.current, buf)
    }

    pub fn put_buffer_stream(&mut self, mut str: strid_t, buf: &[u8]) {
        // Follow chain of echo streams. This does not currently detect cycles:
        // they will result in an infinite loop.
        while str != strid_t::NULL {
            str = self.streams.get_mut(str).unwrap().put_buffer(buf)
        }
    }

    pub fn set_style(&mut self, styl: Style) {
        self.set_style_stream(self.current, styl)
    }

    pub fn set_style_stream(&mut self, mut str: strid_t, styl: Style) {
        // Follow chain of echo streams.
        while str != strid_t::NULL {
            str = self.streams.get_mut(str).unwrap().set_style(styl)
        }
    }

    pub fn get_char_stream(&mut self, str: strid_t) -> i32 {
        let mut buf = [0u8];
        let n = self.get_buffer_stream(str, &mut buf);
        if n != 0 {
            i32::from(buf[0])
        } else {
            -1
        }
    }

    pub fn get_line_stream(&mut self, str: strid_t, buf: &mut [u8]) -> u32 {
        if buf.len() == 0 {
            return 0;
        }
        // This function's behavior is a strange cross-breed of C and Glk expectations:
        //
        // "This reads characters from the given stream, until either len-1 characters have been
        // read or a newline has been read. It then puts a terminal null ('\0') character on the
        // end. It returns the number of characters actually read, including the newline (if there
        // is one) but not including the terminal null."
        let mut idx = 0;
        let mut chbuf = [0u8];
        let maxidx = buf.len() - 1;
        while idx < maxidx {
            let n = self.get_buffer_stream(str, &mut chbuf);
            if n == 0 {
                break;
            }
            buf[idx] = chbuf[0];
            idx += 1;
            if chbuf[0] == b'\n' {
                break;
            }
        }
        buf[idx] = 0;
        idx.try_into().unwrap()
    }

    pub fn get_buffer_stream(&mut self, stream: strid_t, buf: &mut [u8]) -> u32 {
        self.streams.get_mut(stream).unwrap().get_buffer(buf)
    }

    pub fn put_char_uni(&mut self, ch: u32) {
        self.put_buffer_stream_uni(self.current, &[ch])
    }

    pub fn put_buffer_uni(&mut self, buf: &[u32]) {
        self.put_buffer_stream_uni(self.current, buf)
    }

    pub fn put_char_stream_uni(&mut self, _str: strid_t, ch: u32) {
        self.put_buffer_stream_uni(self.current, &[ch])
    }

    pub fn put_buffer_stream_uni(&mut self, mut str: strid_t, buf: &[u32]) {
        while str != strid_t::NULL {
            str = self.streams.get_mut(str).unwrap().put_buffer_uni(buf)
        }
    }

    pub fn get_char_stream_uni(&mut self, str: strid_t) -> i32 {
        let mut buf = [0u32];
        let n = self.get_buffer_stream_uni(str, &mut buf);
        if n != 0 {
            buf[0] as i32
        } else {
            -1
        }
    }

    pub fn get_buffer_stream_uni(&mut self, str: strid_t, buf: &mut [u32]) -> u32 {
        self.streams.get_mut(str).unwrap().get_buffer_uni(buf)
    }

    pub fn get_line_stream_uni(&mut self, str: strid_t, buf: &mut [u32]) -> u32 {
        // 'This reads characters from the given stream, until either len-1 characters have been
        // read or a newline has been read. It then puts a terminal null ('\0') character on the
        // end. It returns the number of characters actually read, including the newline (if there
        // is one) but not including the terminal null."
        let mut idx = 0;
        let mut chbuf = [0u32];
        while (idx + 1) < buf.len() {
            let n = self.get_buffer_stream_uni(str, &mut chbuf);
            if n == 0 {
                break;
            }
            buf[idx] = chbuf[0];
            idx += 1;
            if chbuf[0] == (b'\n' as u32) {
                break;
            }
        }
        buf[idx] = 0;
        idx.try_into().unwrap()
    }

    pub fn get_di_rock(&self, stream: strid_t) -> gidispatch::rock {
        self.streams.get(stream).unwrap().get_di_rock()
    }
}
