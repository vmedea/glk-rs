use crate::helpers::pool;
use crate::types::{frefid_t, schanid_t, strid_t, winid_t};

/** Implement ObjId trait to allow frefid_t to be used as object id in pool. */
impl pool::ObjId for frefid_t {
    const NULL: frefid_t = 0 as frefid_t;
    fn from_index(val: usize) -> Self {
        val.wrapping_add(1) as Self
    }
    fn into_index(self) -> usize {
        (self as usize).wrapping_sub(1)
    }
}

/** Implement ObjId trait to allow schanid_t to be used as object id in pool. */
impl pool::ObjId for schanid_t {
    const NULL: schanid_t = 0 as schanid_t;
    fn from_index(val: usize) -> Self {
        val.wrapping_add(1) as Self
    }
    fn into_index(self) -> usize {
        (self as usize).wrapping_sub(1)
    }
}

/** Implement ObjId trait to allow strid_t to be used as object id in pool. */
impl pool::ObjId for strid_t {
    const NULL: strid_t = 0 as strid_t;
    fn from_index(val: usize) -> Self {
        val.wrapping_add(1) as Self
    }
    fn into_index(self) -> usize {
        (self as usize).wrapping_sub(1)
    }
}

/** Implement ObjId trait to allow winid_t to be used as object id in pool. */
impl pool::ObjId for winid_t {
    const NULL: winid_t = 0 as winid_t;
    fn from_index(val: usize) -> Self {
        val.wrapping_add(1) as Self
    }
    fn into_index(self) -> usize {
        (self as usize).wrapping_sub(1)
    }
}
