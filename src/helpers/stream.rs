/*! Types and functions for stream handling. */
use crate::garglk;
use crate::gidispatch;
use crate::latin1;
use crate::types::{seekmode, stream_result_t, strid_t, style};

/** Style setting enum.
 */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Style {
    Glk(style),
    Zcolor(garglk::zcolor, garglk::zcolor),
    Reverse(u32),
    // TODO Hyperlink(u32),
}

/** Base trait for stream handling.
 */
pub trait Stream {
    /** This function is called before closing the stream. It returns
     * the current stream counters, or None if the stream may not be closed.
     *
     * @note associated Glk function is `glk_stream_close`.
     */
    fn close(&mut self) -> Option<stream_result_t>;

    /** Gets the rock for the stream. This is an arbitrary u32 that
     * was provided on creation.
     *
     * @note associated Glk function is `glk_stream_get_rock`.
     */
    fn get_rock(&self) -> u32;

    /** Set current position for reading/writing in the stream.
     *
     * @note associated Glk function is `glk_stream_set_position`.
     */
    fn set_position(&mut self, pos: i32, mode: seekmode);

    /** Get current position for reading/writing in the stream.
     *
     * @note associated Glk function is `glk_stream_get_position`.
     */
    fn get_position(&mut self) -> u32;

    /** Write to the stream. Optionally returns a stream id to mirror to.
     *
     * @note associated Glk functions are:
     * - `glk_put_char`
     * - `glk_put_char_stream`
     * - `glk_put_buffer`
     * - `glk_put_buffer_stream`
     * - `glk_put_string`
     * - `glk_put_string_stream`
     * These are all specific cases of writing a buffer to the stream.
     */
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t;

    /** Write unicode code points to the stream. Optionally returns a stream id to mirror to.
     *
     * @note associated Glk functions are:
     * - `glk_put_char_uni`
     * - `glk_put_char_stream_uni`
     * - `glk_put_buffer_uni`
     * - `glk_put_buffer_stream_uni`
     * - `glk_put_string_uni`
     * - `glk_put_string_stream_uni`
     * These are all specific cases of writing a unicode buffer to the stream.
     */
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t;

    /** Set current style. Streams that don't have a concept of style can
     * simply ignore this. Optionally returns a stream id to mirror to.
     *
     * @note Associated Glk functions are:
     * - `glk_set_style`
     * - `glk_set_style_stream`
     * - `glk_set_hyperlink` (TODO)
     * - `glk_set_hyperlink_stream` (TODO)
     * - `garglk_set_zcolors`
     * - `garglk_set_zcolors_stream`
     * - `garglk_set_reversevideo`
     * - `garglk_set_reversevideo_stream`
     */
    fn set_style(&mut self, styl: Style) -> strid_t;

    /** Read from the stream.
     *
     * @note Associated Glk functions are:
     * - `glk_get_buffer`
     * - `glk_get_char`
     */
    fn get_buffer(&mut self, buf: &mut [u8]) -> u32;

    /** Read unicode code points from the stream.
     *
     * @note Associated Glk functions are:
     * - `glk_get_buffer_uni`
     * - `glk_get_char_uni`
     */
    fn get_buffer_uni(&mut self, buf: &mut [u32]) -> u32;

    /** Set dispatch rock. This is an arbitrary value stored by the Glk client in the stream
     * object.
     *
     * @note There are no associated Glk functions.
     * This is internal to the gidispatch system used to dispatch Glk functions from a VM.
     */
    fn set_di_rock(&mut self, girock: gidispatch::rock);

    /** Get dispatch rock.
     *
     * @note There are no associated Glk functions.
     * This is internal to the gidispatch system used to dispatch Glk functions from a VM.
     */
    fn get_di_rock(&self) -> gidispatch::rock;
}

/** Fallback implementatino for streams that implement `get_buffer_uni` but not `get_buffer`.
 *
 * @note Is not a default implementation on the trait to prevent accidental circular recursion.
 */
pub fn get_buffer_fallback<T: Stream>(stream: &mut T, buf_out: &mut [u8]) -> u32 {
    // Read into temporary u32 buffer, then convert elements down to u8.
    let mut buf = vec![0u32; buf_out.len()];
    let ret = stream.get_buffer_uni(&mut buf);
    for idx in 0..buf.len() {
        buf_out[idx] = latin1::from_u32(buf[idx]);
    }
    ret
}

/** Fallback implementatino for streams that implement `get_buffer` but not `get_buffer_uni`.
 *
 * @note Is not a default implementation on the trait to prevent accidental circular recursion.
 */
pub fn get_buffer_uni_fallback<T: Stream>(stream: &mut T, buf_out: &mut [u32]) -> u32 {
    // Read into temporary u8 buffer, then convert elements to u32.
    let mut buf = vec![0u8; buf_out.len()];
    let ret = stream.get_buffer(&mut buf);
    for idx in 0..buf.len() {
        buf_out[idx] = u32::from(buf[idx]);
    }
    ret
}

/** Fallback implementatino for streams that implement `put_buffer_uni` but not `put_buffer`.
 *
 * @note Is not a default implementation on the trait to prevent accidental circular recursion.
 */
pub fn put_buffer_fallback<T: Stream>(stream: &mut T, buf: &[u8]) -> strid_t {
    if buf.len() == 1 {
        // Fast no-alloc path for 1-sized buffer.
        stream.put_buffer_uni(&[u32::from(buf[0])])
    } else {
        let v: Vec<u32> = buf.iter().map(|&x| x.into()).collect();
        stream.put_buffer_uni(&v)
    }
}

/** Fallback implementatino for streams that implement `put_buffer` but not `put_buffer_uni`.
 *
 * @note Is not a default implementation on the trait to prevent accidental circular recursion.
 */
pub fn put_buffer_uni_fallback<T: Stream>(stream: &mut T, buf_in: &[u32]) -> strid_t {
    if buf_in.len() == 1 {
        // Fast no-alloc path for 1-sized buffer.
        stream.put_buffer(&[latin1::from_u32(buf_in[0])])
    } else {
        // Convert in temporary u8 buffer.
        // Elements that don't fit into u8 are coerced to '?', per the spec.
        let buf = buf_in
            .iter()
            .copied()
            .map(latin1::from_u32)
            .collect::<Vec<u8>>();
        stream.put_buffer(&buf)
    }
}
