use std::cmp;
use std::convert::TryInto;

use crate::gidispatch;
use crate::helpers::stream::{get_buffer_fallback, get_buffer_uni_fallback, Stream, Style};
use crate::latin1;
use crate::types::{filemode, seekmode, stream_result_t, strid_t, Null};

/** Memory-based stream (u8-based) */
pub struct MemoryStream8 {
    /** Glk rock for stream. */
    rock: u32,
    /** GI dispatch rock for stream. */
    girock: gidispatch::rock,
    /** Buffer. */
    buffer: gidispatch::RetainedBuffer<u8>,
    /** File mode. */
    #[allow(dead_code)]
    mode: filemode,
    /** Current position. */
    pos: usize,
    /** Counters. */
    counters: stream_result_t,
}
impl MemoryStream8 {
    pub fn new(rock: u32, buffer: gidispatch::RetainedBuffer<u8>, mode: filemode) -> Self {
        Self {
            rock,
            girock: gidispatch::rock::DUMMY,
            buffer,
            mode,
            pos: 0,
            counters: stream_result_t {
                readcount: 0,
                writecount: 0,
            },
        }
    }
}
impl Stream for MemoryStream8 {
    fn close(&mut self) -> Option<stream_result_t> {
        Some(self.counters)
    }
    fn get_rock(&self) -> u32 {
        self.rock
    }
    fn set_position(&mut self, pos: i32, mode: seekmode) {
        let pos = pos as isize;
        let len = self.buffer.len() as isize;
        let newpos = match mode {
            seekmode::Start => pos,
            seekmode::End => len + pos,
            seekmode::Current => self.pos as isize + pos,
            _ => self.pos as isize, // unknown mode
        };
        self.pos = cmp::min(cmp::max(newpos, 0), len) as usize;
    }
    fn get_position(&mut self) -> u32 {
        self.pos.try_into().unwrap()
    }
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t {
        self.counters.writecount += buf.len() as u32;
        let last = cmp::min(self.pos + buf.len(), self.buffer.len());
        if self.pos < last {
            self.buffer[self.pos..last].copy_from_slice(&buf[0..last - self.pos]);
            self.pos = last;
        }
        strid_t::NULL
    }
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t {
        self.counters.writecount += buf.len() as u32;
        for &ch in buf {
            let ch: u8 = latin1::from_u32(ch);
            if self.pos < self.buffer.len() {
                self.buffer[self.pos] = ch;
                self.pos += 1;
            }
        }
        strid_t::NULL
    }
    fn set_style(&mut self, _styl: Style) -> strid_t {
        // Ignored for memory streams
        strid_t::NULL
    }
    fn get_buffer(&mut self, buf: &mut [u8]) -> u32 {
        let last = cmp::min(self.pos + buf.len(), self.buffer.len());
        if self.pos < last {
            &buf[0..last - self.pos].copy_from_slice(&self.buffer[self.pos..last]);
            let num = last - self.pos;
            self.pos = last;
            self.counters.readcount += num as u32;
            num.try_into().unwrap()
        } else {
            0
        }
    }
    fn get_buffer_uni(&mut self, buf: &mut [u32]) -> u32 {
        get_buffer_uni_fallback(self, buf)
    }
    fn set_di_rock(&mut self, girock: gidispatch::rock) {
        self.girock = girock;
    }
    fn get_di_rock(&self) -> gidispatch::rock {
        self.girock
    }
}

/** Memory-based stream (u32-based) */
pub struct MemoryStream32 {
    /** Glk rock for stream. */
    rock: u32,
    /** GI dispatch rock for stream. */
    girock: gidispatch::rock,
    /** Buffer. */
    buffer: gidispatch::RetainedBuffer<u32>,
    /** File mode. */
    #[allow(dead_code)]
    mode: filemode,
    /** Current position. */
    pos: usize,
    /** Counters. */
    counters: stream_result_t,
}
impl MemoryStream32 {
    pub fn new(rock: u32, buffer: gidispatch::RetainedBuffer<u32>, mode: filemode) -> Self {
        Self {
            rock,
            girock: gidispatch::rock::DUMMY,
            buffer,
            mode: mode,
            pos: 0,
            counters: stream_result_t {
                readcount: 0,
                writecount: 0,
            },
        }
    }
}
impl Stream for MemoryStream32 {
    fn close(&mut self) -> Option<stream_result_t> {
        Some(self.counters)
    }
    fn get_rock(&self) -> u32 {
        self.rock
    }
    fn set_position(&mut self, pos: i32, mode: seekmode) {
        let pos = pos as isize;
        let len = self.buffer.len() as isize;
        let newpos = match mode {
            seekmode::Start => pos,
            seekmode::End => len + pos,
            seekmode::Current => self.pos as isize + pos,
            _ => self.pos as isize, // unknown mode
        };
        self.pos = cmp::min(cmp::max(newpos, 0), len) as usize;
    }
    fn get_position(&mut self) -> u32 {
        self.pos.try_into().unwrap()
    }
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t {
        self.counters.writecount += buf.len() as u32;
        for &ch in buf {
            if self.pos < self.buffer.len() {
                self.buffer[self.pos] = u32::from(ch);
                self.pos += 1;
            }
        }
        strid_t::NULL
    }
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t {
        self.counters.writecount += buf.len() as u32;
        let last = cmp::min(self.pos + buf.len(), self.buffer.len());
        if self.pos < last {
            self.buffer[self.pos..last].copy_from_slice(&buf[0..last - self.pos]);
            self.pos = last;
        }
        strid_t::NULL
    }
    fn set_style(&mut self, _styl: Style) -> strid_t {
        // Ignored for memory streams
        strid_t::NULL
    }
    fn get_buffer(&mut self, buf: &mut [u8]) -> u32 {
        get_buffer_fallback(self, buf)
    }
    fn get_buffer_uni(&mut self, buf: &mut [u32]) -> u32 {
        let last = cmp::min(self.pos + buf.len(), self.buffer.len());
        if self.pos < last {
            &buf[0..last - self.pos].copy_from_slice(&self.buffer[self.pos..last]);
            let num = last - self.pos;
            self.pos = last;
            self.counters.readcount += num as u32;
            num.try_into().unwrap()
        } else {
            0
        }
    }
    fn set_di_rock(&mut self, girock: gidispatch::rock) {
        self.girock = girock;
    }
    fn get_di_rock(&self) -> gidispatch::rock {
        self.girock
    }
}
