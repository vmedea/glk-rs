use std::convert::TryInto;
use std::fs::File;
use std::io::{Read, Seek, SeekFrom, Write};

use crate::gidispatch;
use crate::types::{seekmode, stream_result_t, strid_t, Null};
use crate::{latin1, unicode};

use crate::helpers::stream::{
    get_buffer_fallback, get_buffer_uni_fallback, put_buffer_fallback, put_buffer_uni_fallback,
    Stream, Style,
};

/** Read one UTF-8 character from a stream. This is more complicated than it sounds: as these have
 * a variable size, we don't know how much to read from the underlying stream in advance.
 */
pub fn read_utf8_char<T: Read>(file: &mut T) -> Option<u32> {
    let mut ubuf = [0u8, 0u8, 0u8, 0u8];
    if file.read(&mut ubuf[0..1]).unwrap() == 1 {
        // Determine number of additional characters needed.
        if (ubuf[0] & 0b1000_0000) == 0b0000_0000 {
            Some(u32::from(ubuf[0]))
        } else if (ubuf[0] & 0b1110_0000) == 0b1100_0000 {
            if file.read(&mut ubuf[1..2]).unwrap_or(0) == 1 {
                Some(u32::from(ubuf[0] & 0b0001_1111) << 6 | u32::from(ubuf[1] & 0b0011_1111))
            } else {
                None // Read error or EOF in sequence
            }
        } else if (ubuf[0] & 0b1111_0000) == 0b1110_0000 {
            if file.read(&mut ubuf[1..3]).unwrap_or(0) == 2 {
                Some(
                    u32::from(ubuf[0] & 0b0000_1111) << 12
                        | u32::from(ubuf[1] & 0b0011_1111) << 6
                        | u32::from(ubuf[2] & 0b0011_1111),
                )
            } else {
                None // Read error or EOF in sequence
            }
        } else if (ubuf[0] & 0b1111_1000) == 0b1111_0000 {
            if file.read(&mut ubuf[1..4]).unwrap_or(0) == 3 {
                Some(
                    u32::from(ubuf[0] & 0b0000_0111) << 18
                        | u32::from(ubuf[1] & 0b0011_1111) << 12
                        | u32::from(ubuf[2] & 0b0011_1111) << 6
                        | u32::from(ubuf[3] & 0b0011_1111),
                )
            } else {
                None // Read error or EOF in sequence
            }
        } else {
            None // Invalid unicode
        }
    } else {
        None // Read error or EOF in sequence
    }
}

/** File stream (8 bit binary). */
pub struct FileStreamBin8 {
    /** Glk rock for main stream. */
    rock: u32,
    /** GI dispatch rock for stream. */
    girock: gidispatch::rock,
    /** Backed file. */
    file: File,
}
impl FileStreamBin8 {
    pub fn new(rock: u32, file: File) -> Self {
        Self {
            rock,
            girock: gidispatch::rock::DUMMY,
            file,
        }
    }
}
impl Stream for FileStreamBin8 {
    fn close(&mut self) -> Option<stream_result_t> {
        // The actual closing happens when the struct is dropped in remove().
        Some(stream_result_t {
            readcount: 0,
            writecount: 0,
        })
    }
    fn get_rock(&self) -> u32 {
        self.rock
    }
    fn set_position(&mut self, pos: i32, mode: seekmode) {
        self.file
            .seek(match mode {
                seekmode::Start => SeekFrom::Start(pos as u64),
                seekmode::Current => SeekFrom::Current(pos as i64),
                seekmode::End => SeekFrom::End(pos as i64),
                _ => {
                    panic!("Invalid seekmode");
                }
            })
            .unwrap();
    }
    fn get_position(&mut self) -> u32 {
        self.file
            .seek(SeekFrom::Current(0))
            .unwrap()
            .try_into()
            .unwrap()
    }
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t {
        self.file.write_all(buf).unwrap();
        strid_t::NULL
    }
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t {
        put_buffer_uni_fallback(self, buf)
    }
    fn set_style(&mut self, _styl: Style) -> strid_t {
        // Safe to ignore.
        strid_t::NULL
    }
    fn get_buffer(&mut self, buf: &mut [u8]) -> u32 {
        self.file.read(buf).unwrap().try_into().unwrap()
    }
    fn get_buffer_uni(&mut self, buf: &mut [u32]) -> u32 {
        get_buffer_uni_fallback(self, buf)
    }
    fn set_di_rock(&mut self, girock: gidispatch::rock) {
        self.girock = girock;
    }
    fn get_di_rock(&self) -> gidispatch::rock {
        self.girock
    }
}

/** File stream (32 bit big-endian binary). */
pub struct FileStreamBin32 {
    /** Glk rock for main stream. */
    rock: u32,
    /** GI dispatch rock for stream. */
    girock: gidispatch::rock,
    /** Backed file. */
    file: File,
}
impl FileStreamBin32 {
    pub fn new(rock: u32, file: File) -> Self {
        Self {
            rock,
            girock: gidispatch::rock::DUMMY,
            file,
        }
    }
}
impl Stream for FileStreamBin32 {
    fn close(&mut self) -> Option<stream_result_t> {
        // The actual closing happens when the struct is dropped in remove().
        Some(stream_result_t {
            readcount: 0,
            writecount: 0,
        })
    }
    fn get_rock(&self) -> u32 {
        self.rock
    }
    fn set_position(&mut self, pos: i32, mode: seekmode) {
        self.file
            .seek(match mode {
                seekmode::Start => SeekFrom::Start((pos as u64) * 4),
                seekmode::Current => SeekFrom::Current((pos as i64) * 4),
                seekmode::End => SeekFrom::End((pos as i64) * 4),
                _ => {
                    panic!("Invalid seekmode");
                }
            })
            .unwrap();
    }
    fn get_position(&mut self) -> u32 {
        (self.file.seek(SeekFrom::Current(0)).unwrap() / 4)
            .try_into()
            .unwrap()
    }
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t {
        put_buffer_fallback(self, buf)
    }
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t {
        let temp: Vec<[u8; 4]> = buf.iter().map(|x| x.to_be_bytes()).collect();
        let v: Vec<u8> = temp.iter().flatten().copied().collect();
        self.file.write_all(&v).unwrap();
        strid_t::NULL
    }
    fn set_style(&mut self, _styl: Style) -> strid_t {
        // Safe to ignore.
        strid_t::NULL
    }
    fn get_buffer(&mut self, buf: &mut [u8]) -> u32 {
        get_buffer_fallback(self, buf)
    }
    fn get_buffer_uni(&mut self, buf: &mut [u32]) -> u32 {
        // Read into byte array, then convert each element from bytes
        // to big-endian u32.
        let mut v = vec![0u8; buf.len() * 4];
        let n = self.file.read(&mut v).unwrap();
        for (idx, chunk) in v[0..n].chunks_exact(4).enumerate() {
            buf[idx] = (u32::from(chunk[0]) << 24)
                | (u32::from(chunk[1]) << 16)
                | (u32::from(chunk[2]) << 8)
                | (u32::from(chunk[3]) << 0);
        }
        (n / 4).try_into().unwrap()
    }
    fn set_di_rock(&mut self, girock: gidispatch::rock) {
        self.girock = girock;
    }
    fn get_di_rock(&self) -> gidispatch::rock {
        self.girock
    }
}

/** File stream (UTF-8). */
pub struct FileStreamUTF8 {
    /** Glk rock for main stream. */
    rock: u32,
    /** GI dispatch rock for stream. */
    girock: gidispatch::rock,
    /** Backed file. */
    file: File,
}
impl FileStreamUTF8 {
    pub fn new(rock: u32, file: File) -> Self {
        Self {
            rock,
            girock: gidispatch::rock::DUMMY,
            file,
        }
    }
}
impl Stream for FileStreamUTF8 {
    fn close(&mut self) -> Option<stream_result_t> {
        // The actual closing happens when the struct is dropped in remove().
        Some(stream_result_t {
            readcount: 0,
            writecount: 0,
        })
    }
    fn get_rock(&self) -> u32 {
        self.rock
    }
    fn set_position(&mut self, pos: i32, mode: seekmode) {
        self.file
            .seek(match mode {
                seekmode::Start => SeekFrom::Start(pos as u64),
                seekmode::Current => SeekFrom::Current(pos as i64),
                seekmode::End => SeekFrom::End(pos as i64),
                _ => {
                    panic!("Invalid seekmode");
                }
            })
            .unwrap();
    }
    fn get_position(&mut self) -> u32 {
        self.file
            .seek(SeekFrom::Current(0))
            .unwrap()
            .try_into()
            .unwrap()
    }
    fn put_buffer(&mut self, buf: &[u8]) -> strid_t {
        let s = latin1::to_string(buf);
        self.file.write_all(s.as_bytes()).unwrap();
        strid_t::NULL
    }
    fn put_buffer_uni(&mut self, buf: &[u32]) -> strid_t {
        let s = unicode::to_string(buf);
        self.file.write_all(s.as_bytes()).unwrap();
        strid_t::NULL
    }
    fn set_style(&mut self, _styl: Style) -> strid_t {
        // Safe to ignore.
        strid_t::NULL
    }
    fn get_buffer(&mut self, buf: &mut [u8]) -> u32 {
        get_buffer_fallback(self, buf)
    }
    fn get_buffer_uni(&mut self, buf: &mut [u32]) -> u32 {
        let mut idx = 0;
        while idx < buf.len() {
            if let Some(val) = read_utf8_char(&mut self.file) {
                buf[idx] = val;
                idx += 1;
            } else {
                break;
            }
        }
        idx.try_into().unwrap()
    }
    fn set_di_rock(&mut self, girock: gidispatch::rock) {
        self.girock = girock;
    }
    fn get_di_rock(&self) -> gidispatch::rock {
        self.girock
    }
}
