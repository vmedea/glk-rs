/*! Mostly safe rust traits for implementing the Glk spec, including optional modules.
 (https://www.eblong.com/zarf/glk/glk-spec-075_4.html).
*/
use crate::types::*;
use crate::{ext, garglk, giblorb, gidispatch};

/** Mandatory Glk API functions. */
pub trait Base {
    /** Set interrupt handler (this is called before process shutdown).
     *
     * @note Cannot be invoked through the dispatch layer, and is never called by glulxe.
     */
    fn set_interrupt_handler(&mut self, func: Option<unsafe extern "C" fn()>);

    /** Tick handler. This is intended to be called often by the Glk program,
     * for example between every opcode for a VM interpreter.
     */
    fn tick(&mut self);

    /** Query Glk API capabilities. This lets the Glk program knows which functionality is
     * implemented by the library. See `gestalt` documentation for a list of specific
     * capabilitites.
     */
    fn gestalt(&mut self, sel: gestalt, val: u32, arr: &mut [u32]) -> u32;

    /* Latin-1 character handling */
    /** Convert one Latin-1 character to lowercase. */
    fn char_to_lower(&mut self, ch: u8) -> u8;

    /** Convert one Latin-1 character to uppercase. */
    fn char_to_upper(&mut self, ch: u8) -> u8;

    /* Window handling */
    /** This returns the root window. If there are no windows, this returns NULL. */
    fn window_get_root(&mut self) -> winid_t;

    /** Open a new window, returning an opaque handle to the window. If no new window could be
     * opened, return NULL. */
    fn window_open(
        &mut self,
        split: winid_t,
        method: winmethod,
        size: u32,
        wtype: wintype,
        rock: u32,
    ) -> winid_t;

    /** Close a window. */
    fn window_close(&mut self, win: winid_t) -> stream_result_t;

    /** Get dimensions of a window. May be inexact for text buffers, but not for text grids.
     * Returns (width, height). */
    fn window_get_size(&mut self, win: winid_t) -> (u32, u32);

    /** Change arrangement of windows. */
    fn window_set_arrangement(
        &mut self,
        win: winid_t,
        method: winmethod,
        size: u32,
        keywin: winid_t,
    );

    /** Get arrangement of windows. Returns (method, size, keywin) */
    fn window_get_arrangement(&mut self, win: winid_t) -> (u32, u32, winid_t);

    /** Iterate over windows. Returns (winid, rock) */
    fn window_iterate(&mut self, win: winid_t) -> (winid_t, u32);

    /** Get rock value for a window. */
    fn window_get_rock(&mut self, win: winid_t) -> u32;

    /** Get the type of a window. */
    fn window_get_type(&mut self, win: winid_t) -> wintype;

    /** Get the parent of a window. */
    fn window_get_parent(&mut self, win: winid_t) -> winid_t;

    /** Get next sibling of a window. */
    fn window_get_sibling(&mut self, win: winid_t) -> winid_t;

    /** Erase the window contents. The meaning of this depends on the window type. */
    fn window_clear(&mut self, win: winid_t);

    /** Set the current cursor position (for text grids). */
    fn window_move_cursor(&mut self, win: winid_t, xpos: u32, ypos: u32);

    /** Return the stream which is associated with the window. */
    fn window_get_stream(&mut self, win: winid_t) -> strid_t;

    /** Set the echo stream for a window. The echo stream gets a copy of everything
     * printed to the window. */
    fn window_set_echo_stream(&mut self, win: winid_t, str: strid_t);

    /** Get the echo stream for a window. */
    fn window_get_echo_stream(&mut self, win: winid_t) -> strid_t;

    /** Set current window. This is a shortcut for `stream_set_current(window_get_stream(win))`. */
    fn set_window(&mut self, win: winid_t);

    /* Stream handling (Latin-1) */
    /** Open a file as a stream. */
    fn stream_open_file(&mut self, fileref: frefid_t, fmode: filemode, rock: u32) -> strid_t;

    /** Open a buffer in memory as a stream. */
    fn stream_open_memory(
        &mut self,
        buf: &mut gidispatch::RetainableBuffer<u8>,
        fmode: filemode,
        rock: u32,
    ) -> strid_t;

    /** Close a stream. This does not work for window streams. */
    fn stream_close(&mut self, str: strid_t) -> stream_result_t;

    /** Iterate over all streams. Returns (strid, rock). */
    fn stream_iterate(&mut self, str: strid_t) -> (strid_t, u32);

    /** Get the rock value for a stream. */
    fn stream_get_rock(&mut self, str: strid_t) -> u32;

    /** Move the position of the file pointer in a stream. */
    fn stream_set_position(&mut self, str: strid_t, pos: i32, mode: seekmode);

    /** Get the current position of the file pointer in a stream. */
    fn stream_get_position(&mut self, str: strid_t) -> u32;

    /** Set current stream. This is where stream-less commands such as
     * `put_char` will go.
     */
    fn stream_set_current(&mut self, str: strid_t);

    /** Get current stream. */
    fn stream_get_current(&mut self) -> strid_t;

    /** Write one character to the current stream. */
    fn put_char(&mut self, ch: u8);

    /** Write one character to the specified stream. */
    fn put_char_stream(&mut self, str: strid_t, ch: u8);

    /** Write a buffer of unicode characters to current stream.
     *
     * @note `put_string` ends up at this call too.
     */
    fn put_buffer(&mut self, buf: &[u8]);

    /** Write a buffer of characters in the specified stream.
     *
     * @note `put_string_stream` ends up at this call too.
     */
    fn put_buffer_stream(&mut self, str: strid_t, buf: &[u8]);

    /** Set printing style for current stream. */
    fn set_style(&mut self, styl: style);

    /** Set printing style for specified stream. */
    fn set_style_stream(&mut self, str: strid_t, styl: style);

    /** Read a character from the specified stream. */
    fn get_char_stream(&mut self, str: strid_t) -> i32;

    /** Read a line of characters from the specified stream. */
    fn get_line_stream(&mut self, str: strid_t, buf: &mut [u8]) -> u32;

    /** Read a buffer of characters from the specified stream. */
    fn get_buffer_stream(&mut self, str: strid_t, buf: &mut [u8]) -> u32;

    /** Set a style hint (per window type). This requests a style to have a certain
     * style property. */
    fn stylehint_set(&mut self, wtype: wintype, styl: style, hint: stylehint, val: i32);

    /** Clear style hint (per window type). This requests a property on the style to be reset to
     * the default. */
    fn stylehint_clear(&mut self, wtype: wintype, styl: style, hint: stylehint);

    /** Return whether two styles can be distinguished visually. */
    fn style_distinguish(&mut self, win: winid_t, styl1: style, styl2: style) -> u32;

    /** Return a style property for a style. Returns Some(result) if the value is available, None
     * otherwise. */
    fn style_measure(&mut self, win: winid_t, styl: style, hint: stylehint) -> Option<u32>;

    /** Create a temporary file. */
    fn fileref_create_temp(&mut self, usage: fileusage, rock: u32) -> frefid_t;

    /** Create a named file. */
    fn fileref_create_by_name(&mut self, usage: fileusage, name: &[u8], rock: u32) -> frefid_t;

    /** Create a file, prompting the user interactively for the name. */
    fn fileref_create_by_prompt(
        &mut self,
        usage: fileusage,
        fmode: filemode,
        rock: u32,
    ) -> frefid_t;

    /** Create a new file reference from an existing file reference, but with (possibly) different
     * usage. */
    fn fileref_create_from_fileref(
        &mut self,
        usage: fileusage,
        fref: frefid_t,
        rock: u32,
    ) -> frefid_t;

    /** Destroy a file reference. */
    fn fileref_destroy(&mut self, fref: frefid_t);

    /** Iterate over all file references. Returns (fref, rock). */
    fn fileref_iterate(&mut self, fref: frefid_t) -> (frefid_t, u32);

    /** Get rock for a file reference. */
    fn fileref_get_rock(&mut self, fref: frefid_t) -> u32;

    /** Delete a file by reference. */
    fn fileref_delete_file(&mut self, fref: frefid_t);

    /** Check whether a file exists. */
    fn fileref_does_file_exist(&mut self, fref: frefid_t) -> u32;

    /** Wait for next event and return it. */
    fn select(&mut self) -> event_t;

    /** Poll (non-blocking) for next event. Return evtype `None` if
     * there is no event available.
     * The function only checks for `Timer`, and possibly `Arrange` and `SoundNotify` events, not
     * for user input of any kind.
     */
    fn select_poll(&mut self) -> event_t;

    /** Request timer events to be sent every `millisecs` milliseconds, or cancel
     * if `0`.
     */
    fn request_timer_events(&mut self, millisecs: u32);

    /** Request a line input event to be sent. */
    fn request_line_event(
        &mut self,
        win: winid_t,
        buf: &mut gidispatch::RetainableBuffer<u8>,
        initlen: u32,
    );

    /** Request a character event to be sent. */
    fn request_char_event(&mut self, win: winid_t);

    /** Request a mouse event to be sent. */
    fn request_mouse_event(&mut self, win: winid_t);

    /** Cancel requested line event. */
    fn cancel_line_event(&mut self, win: winid_t) -> event_t;

    /** Cancel requested character input event. */
    fn cancel_char_event(&mut self, win: winid_t);

    /** Cancel requested mouse event. */
    fn cancel_mouse_event(&mut self, win: winid_t);
}

/** GLK_MODULE_LINE_ECHO */
pub trait LineEcho {
    /** Surpress line input echo. If the `val` argument is zero, all subsequent line input requests
     * in the given window will leave the buffer unchanged after the input is completed or
     * cancelled; the player's input will not be printed. If `val` is nonzero, subsequent input
     * requests will have the normal printing behavior.
     */
    fn set_echo_line_event(&mut self, win: winid_t, val: u32);
}

/** GLK_MODULE_LINE_TERMINATORS */
pub trait LineTerminators {
    /** Configures that other keystrokes , besides newline, terminate line input
     * events.
     */
    fn set_terminators_line_event(&mut self, win: winid_t, keycodes: &[u32]);
}

/** GLK_MODULE_UNICODE */
pub trait Unicode {
    /** Convert a buffer to lower-case. */
    fn buffer_to_lower_case_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32;

    /** Convert a buffer to upper-case. */
    fn buffer_to_upper_case_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32;

    /** Convert a buffer to title-case (effectively capitalizes the first letter of the buffer). */
    fn buffer_to_title_case_uni(&mut self, buf: &mut [u32], numchars: u32, lowerrest: u32) -> u32;

    /** Write one unicode character to current stream. */
    fn put_char_uni(&mut self, ch: u32);

    /** Write a buffer of unicode characters to current stream.
     *
     * @note `put_string_uni` ends up at this call too.
     */
    fn put_buffer_uni(&mut self, buf: &[u32]);

    /** Write unicode character to arbitrary stream. */
    fn put_char_stream_uni(&mut self, str: strid_t, ch: u32);

    /** Write a buffer of unicode characters to arbitrary stream.
     *
     * @note `put_string_stream_uni` ends up at this call too.
     */
    fn put_buffer_stream_uni(&mut self, str: strid_t, buf: &[u32]);

    /** Read one unicode character from a stream. */
    fn get_char_stream_uni(&mut self, str: strid_t) -> i32;

    /** Read a buffer of unicode characters from a stream. */
    fn get_buffer_stream_uni(&mut self, str: strid_t, buf: &mut [u32]) -> u32;

    /** Read a line of unicode characters from a stream. */
    fn get_line_stream_uni(&mut self, str: strid_t, buf: &mut [u32]) -> u32;

    /** Open a file in unicode mode. */
    fn stream_open_file_uni(&mut self, fileref: frefid_t, fmode: filemode, rock: u32) -> strid_t;

    /** Open a memory stream in unicode mode. Each character is represented as a 32-bit integer. */
    fn stream_open_memory_uni(
        &mut self,
        buf: &mut gidispatch::RetainableBuffer<u32>,
        fmode: filemode,
        rock: u32,
    ) -> strid_t;

    /** Request a character input event returning unicode. */
    fn request_char_event_uni(&mut self, win: winid_t);

    /** Request a line input event returning unicode. */
    fn request_line_event_uni(
        &mut self,
        win: winid_t,
        buf: &mut gidispatch::RetainableBuffer<u32>,
        initlen: u32,
    );
}

/** GLK_MODULE_UNICODE_NORM */
pub trait UnicodeNorm {
    /** This transforms a string into its canonical decomposition ("Normalization Form D"). */
    fn buffer_canon_decompose_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32;

    /** This transforms a string into its canonical decomposition and recomposition ("Normalization
     * Form C"). */
    fn buffer_canon_normalize_uni(&mut self, buf: &mut [u32], numchars: u32) -> u32;
}

/** GLK_MODULE_IMAGE */
pub trait Image {
    /** Draws the given image resource in the given window. The position of the image is
     * given by val1 and val2.
     */
    fn image_draw(&mut self, win: winid_t, image: u32, val1: i32, val2: i32) -> u32;

    /** Draws the given image resource in the given window, scaling it to the given width and
     * height. The position of the image is given by val1 and val2.
     */
    fn image_draw_scaled(
        &mut self,
        win: winid_t,
        image: u32,
        val1: i32,
        val2: i32,
        width: u32,
        height: u32,
    ) -> u32;

    /** Returns information about an image. Returns Some((width, height)), or None if the image
     * does not exist.
     */
    fn image_get_info(&mut self, image: u32) -> Option<(u32, u32)>;

    /** "Break" the stream of text down below the current margin image. */
    fn window_flow_break(&mut self, win: winid_t);

    /** Fills the given rectangle with the window's background color. */
    fn window_erase_rect(&mut self, win: winid_t, left: i32, top: i32, width: u32, height: u32);

    /** Fills the given rectangle with the given color. */
    fn window_fill_rect(
        &mut self,
        win: winid_t,
        color: u32,
        left: i32,
        top: i32,
        width: u32,
        height: u32,
    );

    /** Set the background color of the window. */
    fn window_set_background_color(&mut self, win: winid_t, color: u32);
}

/** GLK_MODULE_SOUND */
pub trait Sound {
    /** Create a sound channel. */
    fn schannel_create(&mut self, rock: u32) -> schanid_t;

    /** Destroy a sound channel. */
    fn schannel_destroy(&mut self, chan: schanid_t);

    /** Iterate over all sound channels. Returns (chan, rock). */
    fn schannel_iterate(&mut self, chan: schanid_t) -> (schanid_t, u32);

    /** Get the stored rock for a sound channel. */
    fn schannel_get_rock(&mut self, chan: schanid_t) -> u32;

    /** Play a sound on a sound channel. */
    fn schannel_play(&mut self, chan: schanid_t, snd: u32) -> u32;

    /** Play a sound on a sound channel, giving number of repeats and whether to notify on
     * completion.
     */
    fn schannel_play_ext(&mut self, chan: schanid_t, snd: u32, repeats: u32, notify: u32) -> u32;

    /** Stop playing sound on a sound channel.
     */
    fn schannel_stop(&mut self, chan: schanid_t);

    /** Set the current volume for a sound channel.
     */
    fn schannel_set_volume(&mut self, chan: schanid_t, vol: u32);

    /** Gives the library a hint about whether the given sound should be loaded or not.
     */
    fn sound_load_hint(&mut self, snd: u32, flag: u32);
}

/** GLK_MODULE_SOUND2 */
pub trait Sound2 {
    /** Create a sound channel, giving the initial volume. */
    fn schannel_create_ext(&mut self, rock: u32, volume: u32) -> schanid_t;

    /** Play multiple sounds at the same time on multiple channels. Optionally notify on
     * completion.
     */
    fn schannel_play_multi(
        &mut self,
        chanarray: &[schanid_t],
        sndarray: &[u32],
        notify: u32,
    ) -> u32;

    /** Pause playing on a channel.
     */
    fn schannel_pause(&mut self, chan: schanid_t);

    /** Unpause playing on a channel.
     */
    fn schannel_unpause(&mut self, chan: schanid_t);

    /** Set volume of a channel with duration (fade in or out) and optional notification.
     */
    fn schannel_set_volume_ext(&mut self, chan: schanid_t, vol: u32, duration: u32, notify: u32);
}

/** GLK_MODULE_HYPERLINKS */
pub trait Hyperlinks {
    /** Set the current link value in the current output stream. A link value is any non-zero
     * integer; zero indicates no link. Subsequent text output is considered to make up the body
     * of the link, which continues until the link value is changed (or set to zero). */
    fn set_hyperlink(&mut self, linkval: u32);

    /** Set the current link value in an arbitrary output stream. */
    fn set_hyperlink_stream(&mut self, str: strid_t, linkval: u32);

    /** Request an event when the user clicks a hyperlink. */
    fn request_hyperlink_event(&mut self, win: winid_t);

    /** Stop requesting an event when the user clicks a hyperlink. */
    fn cancel_hyperlink_event(&mut self, win: winid_t);
}

/** GLK_MODULE_DATETIME */
pub trait Datetime {
    /** Get the current (wall clock) time. */
    fn current_time(&mut self) -> glktimeval_t;

    /** Get the current (wall clock) time as a "simple time", which is a single 32-bit integer,
     * that is the UNIX time in seconds divided by the given factor.
     */
    fn current_simple_time(&mut self, factor: u32) -> i32;

    /** Convert a time to a broken-out date structure (in UTC). */
    fn time_to_date_utc(&mut self, time: glktimeval_t) -> glkdate_t;

    /** Convert a time to a broken-out date structure (as local time). */
    fn time_to_date_local(&mut self, time: glktimeval_t) -> glkdate_t;

    /** Convert a "simple time" to a broken-out date structure (in UTC). */
    fn simple_time_to_date_utc(&mut self, time: i32, factor: u32) -> glkdate_t;

    /** Convert a "simple time" to a broken-out date structure (in local time). */
    fn simple_time_to_date_local(&mut self, time: i32, factor: u32) -> glkdate_t;

    /** Convert a broken-out date structure (in UTC) to a time structure. */
    fn date_to_time_utc(&mut self, date: glkdate_t) -> glktimeval_t;

    /** Convert a broken-out date structure (in local time) to a time structure. */
    fn date_to_time_local(&mut self, date: glkdate_t) -> glktimeval_t;

    /** Convert a broken-out date structure (in UTC) to a "simple time". */
    fn date_to_simple_time_utc(&mut self, date: glkdate_t, factor: u32) -> i32;

    /** Convert a broken-out date structure (in local time) to a "simple time". */
    fn date_to_simple_time_local(&mut self, date: glkdate_t, factor: u32) -> i32;
}

/** GLK_MODULE_RESOURCE_STREAM */
pub trait ResourceStream {
    /** Open a resource in the game blorb in 8-bit mode. */
    fn stream_open_resource(&mut self, filenum: u32, rock: u32) -> strid_t;

    /** Open a resource in the game blorb in unicode mode. */
    fn stream_open_resource_uni(&mut self, filenum: u32, rock: u32) -> strid_t;
}

/** Dispatch trait for all Glk APIs. Each function returns a
 * reference to a trait which will be called for functions on the associated
 * Glk module.
 */
pub trait Api {
    /** Mandatory Glk API functions. */
    fn base(&mut self) -> &mut dyn Base;
    /** GLK_MODULE_LINE_ECHO */
    fn line_echo(&mut self) -> Option<&mut dyn LineEcho> {
        None
    }
    /** GLK_MODULE_LINE_TERMINATORS */
    fn line_terminators(&mut self) -> Option<&mut dyn LineTerminators> {
        None
    }
    /** GLK_MODULE_UNICODE */
    fn unicode(&mut self) -> Option<&mut dyn Unicode> {
        None
    }
    /** GLK_MODULE_UNICODE_NORM */
    fn unicode_norm(&mut self) -> Option<&mut dyn UnicodeNorm> {
        None
    }
    /** GLK_MODULE_IMAGE */
    fn image(&mut self) -> Option<&mut dyn Image> {
        None
    }
    /** GLK_MODULE_SOUND */
    fn sound(&mut self) -> Option<&mut dyn Sound> {
        None
    }
    /** GLK_MODULE_SOUND2 */
    fn sound2(&mut self) -> Option<&mut dyn Sound2> {
        None
    }
    /** GLK_MODULE_HYPERLINKS */
    fn hyperlinks(&mut self) -> Option<&mut dyn Hyperlinks> {
        None
    }
    /** GLK_MODULE_DATETIME */
    fn date_time(&mut self) -> Option<&mut dyn Datetime> {
        None
    }
    /** GLK_MODULE_RESOURCE_STREAM */
    fn resource_stream(&mut self) -> Option<&mut dyn ResourceStream> {
        None
    }
    /** GLK_MODULE_GARGLKTEXT */
    fn garglk_text(&mut self) -> Option<&mut dyn garglk::GarGlkText> {
        None
    }
    /** GI blorb handler functions */
    fn giblorb(&mut self) -> Option<&mut dyn giblorb::Handlers> {
        None
    }
    /** GI dispatch handler functions */
    fn gidispatch(&mut self) -> Option<&mut dyn gidispatch::Handlers> {
        None
    }
    /** Rust Glk extension handler functions */
    fn ext(&mut self) -> Option<&mut dyn ext::Handlers> {
        None
    }
}
