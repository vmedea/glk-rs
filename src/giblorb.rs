#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use glk_sys::giblorb_create_map;

use crate::types::strid_t;
use crate::util::unhandled;

pub use glk_sys::giblorb_map_t;

/** Newtype for giblorb error */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct giblorb_err(pub u32);
impl giblorb_err {
    /** No error. */
    pub const None: Self = Self(0);
    /** Something is compiled wrong in the Blorb layer. */
    pub const CompileTime: Self = Self(1);
    /** Memory could not be allocated. */
    pub const Alloc: Self = Self(2);
    /** Data could not be read from the file. */
    pub const Read: Self = Self(3);
    /** The map parameter is invalid. */
    pub const NotAMap: Self = Self(4);
    /** The Blorb file is corrupted or invalid. */
    pub const Format: Self = Self(5);
    /** The requested data could not be found. */
    pub const NotFound: Self = Self(6);
}

/** Methods for loading a chunk. */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct giblorb_method(pub u32);
impl giblorb_method {
    /** No data is actually loaded in. You can use this if you are only interested in whether a
     * chunk exists, or in the chunknum and length parameters.
     */
    pub const DontLoad: Self = Self(0);
    /** `data.ptr` is filled with a pointer to allocated memory containing the chunk data. This
     * memory is owned by the map, not you.
     */
    pub const Memory: Self = Self(1);
    /** `data.startpos` is filled in with the file position of the chunk data. You can use
     * `glk_stream_set_position()` to read the data from the stream.
     */
    pub const FilePos: Self = Self(2);
}

/** GI blorb handler functions */
pub trait Handlers {
    fn giblorb_set_resource_map(&mut self, _file: strid_t) -> giblorb_err {
        unhandled("giblorb_set_resource_map");
        giblorb_err::None
    }

    fn giblorb_get_resource_map(&mut self) -> *mut giblorb_map_t {
        unhandled("giblorb_get_resource_map");
        0 as *mut giblorb_map_t
    }
}

/** Create a resource map from a blorb file, represented as Glk stream. */
pub fn create_map(file: strid_t) -> Result<*mut giblorb_map_t, giblorb_err> {
    let mut ret = 0 as *mut giblorb_map_t;
    let err = unsafe {
        giblorb_err(giblorb_create_map(
            file,
            &mut ret as *mut *mut giblorb_map_t,
        ))
    };
    if err != giblorb_err::None {
        Err(err)
    } else {
        Ok(ret)
    }
}
