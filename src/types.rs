#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
/*! Type definitions for Glk API types. These are intentionally not enums because
 * there is the possibility of extensions defining new values (Glk explicitly mentions that
 * unknown values are to be ignored, and not result in error), as well as some types being
 * bitfields, which would be unwieldy as enum.
 */

/** Import Null trait. */
pub use glk_sys::Null;
/** Import struct types from C binding. */
pub use glk_sys::{event_t, glkdate_t, glktimeval_t, stream_result_t};
/** Import opaque handle types from C binding. */
pub use glk_sys::{frefid_t, schanid_t, strid_t, winid_t};

/** Newtype for Glk gestalt */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct gestalt(pub u32);
impl gestalt {
    /** Glk version number. */
    pub const Version: Self = Self(0);
    /** Character codes that can be typed by the player as part of character input. */
    pub const CharInput: Self = Self(1);
    /** Character codes that can be typed by the player as part of line input. */
    pub const LineInput: Self = Self(2);
    /** Character codes that can be shown as part of output. */
    pub const CharOutput: Self = Self(3);
    /** Is mouse input supported? */
    pub const MouseInput: Self = Self(4);
    /** Does the library support timer events? */
    pub const Timer: Self = Self(5);
    /** Does the library support graphics? */
    pub const Graphics: Self = Self(6);
    /** Whether images can be drawn in windows of a certain type. */
    pub const DrawImage: Self = Self(7);
    /** Does the library support sound at all? */
    pub const Sound: Self = Self(8);
    /** The `glk_schannel_set_volume()` function works. */
    pub const SoundVolume: Self = Self(9);
    /** Sound notification events are supported. */
    pub const SoundNotify: Self = Self(10);
    /** Hyperlinks are supported. */
    pub const Hyperlinks: Self = Self(11);
    /** Hyperlink input events are supported. */
    pub const HyperlinkInput: Self = Self(12);
    /** Whether MOD songs -- the only music format that Blorb currently supports can be played. */
    pub const SoundMusic: Self = Self(13);
    /** Images with alpha channels can actually be drawn with the appropriate degree of
     * transparency. */
    pub const GraphicsTransparency: Self = Self(14);
    /** Core Unicode functions are available. */
    pub const Unicode: Self = Self(15);
    /** Unicode normalization functions are available. */
    pub const UnicodeNorm: Self = Self(16);
    /** Supports `glk_set_echo_line_event()`. */
    pub const LineInputEcho: Self = Self(17);
    /** `glk_set_terminators_line_event()` is supported. */
    pub const LineTerminators: Self = Self(18);
    /** Query which keycodes can be passed to `glk_set_terminators_line_event()`. */
    pub const LineTerminatorKey: Self = Self(19);
    /** DateTime Glk functions are supported. */
    pub const DateTime: Self = Self(20);
    /** Sound2 functions are supported. */
    pub const Sound2: Self = Self(21);
    /** Resource stream functions are supported. */
    pub const ResourceStream: Self = Self(22);
    /** Character input can be used on graphics windows. */
    pub const GraphicsCharInput: Self = Self(23);
    /** Gargoyle Glk text extensions. */
    pub const GarglkText: Self = Self(0x1100);
}

/** Newtype for Glk gestalt_CharOutput */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct gestalt_CharOutput(pub u32);
impl gestalt_CharOutput {
    /** The character cannot be meaningfully printed. If you try, the player may see nothing, or
     * may see a placeholder. */
    pub const CannotPrint: Self = Self(0);
    /** The library will print some approximation of the character. It will be more or less right,
     * but it may not be precise, and it may not be distinguishable from other, similar characters.
     */
    pub const ApproxPrint: Self = Self(1);
    /** The character will be printed exactly as defined.
     */
    pub const ExactPrint: Self = Self(2);
}

/** Newtype for Glk evtype */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct evtype(pub u32);
impl evtype {
    /** No event. This is a placeholder, and `glk_select()` never returns it (but
     * `glk_select_poll()` might). */
    pub const None: Self = Self(0);
    /** An event that repeats at fixed intervals. */
    pub const Timer: Self = Self(1);
    /** A keystroke event in a window. */
    pub const CharInput: Self = Self(2);
    /** A full line of input completed in a window. */
    pub const LineInput: Self = Self(3);
    /** A mouse click in a window. */
    pub const MouseInput: Self = Self(4);
    /** An event signalling that the sizes of some windows have changed. */
    pub const Arrange: Self = Self(5);
    /** An event signalling that graphics windows must be redrawn. */
    pub const Redraw: Self = Self(6);
    /** The completion of a sound being played in a sound channel. */
    pub const SoundNotify: Self = Self(7);
    /** The selection of a hyperlink in a window. */
    pub const Hyperlink: Self = Self(8);
    /** The completion of a gradual volume change in a sound channel. */
    pub const VolumeNotify: Self = Self(9);
}

/** Newtype for Glk keycode */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct keycode(pub u32);
impl keycode {
    pub const Unknown: Self = Self(0xffffffff);
    /** Left arrow key. */
    pub const Left: Self = Self(0xfffffffe);
    /** Right arrow key. */
    pub const Right: Self = Self(0xfffffffd);
    /** Up arrow key. */
    pub const Up: Self = Self(0xfffffffc);
    /** Down arrow key. */
    pub const Down: Self = Self(0xfffffffb);
    /** Return or enter. */
    pub const Return: Self = Self(0xfffffffa);
    /** Delete or backspace (yuck). */
    pub const Delete: Self = Self(0xfffffff9);
    /** Escape key. */
    pub const Escape: Self = Self(0xfffffff8);
    /** Tab key. */
    pub const Tab: Self = Self(0xfffffff7);
    /** Page up key. */
    pub const PageUp: Self = Self(0xfffffff6);
    /** Page down key. */
    pub const PageDown: Self = Self(0xfffffff5);
    /** Home key. */
    pub const Home: Self = Self(0xfffffff4);
    /** End key. */
    pub const End: Self = Self(0xfffffff3);
    /** F1 key. */
    pub const Func1: Self = Self(0xffffffef);
    /** F2 key. */
    pub const Func2: Self = Self(0xffffffee);
    /** F3 key. */
    pub const Func3: Self = Self(0xffffffed);
    /** F4 key. */
    pub const Func4: Self = Self(0xffffffec);
    /** F5 key. */
    pub const Func5: Self = Self(0xffffffeb);
    /** F6 key. */
    pub const Func6: Self = Self(0xffffffea);
    /** F7 key. */
    pub const Func7: Self = Self(0xffffffe9);
    /** F8 key. */
    pub const Func8: Self = Self(0xffffffe8);
    /** F9 key. */
    pub const Func9: Self = Self(0xffffffe7);
    /** F10 key. */
    pub const Func10: Self = Self(0xffffffe6);
    /** F11 key. */
    pub const Func11: Self = Self(0xffffffe5);
    /** F12 key. */
    pub const Func12: Self = Self(0xffffffe4);

    /** Unicode character. */
    pub fn from_char(ch: char) -> Self {
        Self(u32::from(ch))
    }
}

/** Newtype for Glk style */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct style(pub u32);
impl style {
    /** The style of normal or body text. A new window or stream always starts with style_Normal as
     * the current style. */
    pub const Normal: Self = Self(0);
    /** Text which is emphasized. */
    pub const Emphasized: Self = Self(1);
    /** Text which has a particular arrangement of characters. */
    pub const Preformatted: Self = Self(2);
    /** Text which introduces a large section. This is suitable for the title of an entire game, or
     * a major division such as a chapter. */
    pub const Header: Self = Self(3);
    /** Text which introduces a smaller section within a large section. */
    pub const Subheader: Self = Self(4);
    /** Text which warns of a dangerous condition, or one which the player should pay attention to. */
    pub const Alert: Self = Self(5);
    /** Text which notifies of an interesting condition. */
    pub const Note: Self = Self(6);
    /** Text which forms a quotation or otherwise abstracted text. */
    pub const BlockQuote: Self = Self(7);
    /** Text which the player has entered. You should generally not use this style at all, except
     * when simulating user input from a file. */
    pub const Input: Self = Self(8);
    /** This style has no particular semantic meaning. You may define a meaning relevant to your
     * own work, and use it as you see fit. */
    pub const User1: Self = Self(9);
    /** Another style available for your use. */
    pub const User2: Self = Self(10);
    /** Number of pre-defined styles, for defining fixed-size arrays. */
    pub const NUMSTYLES: Self = Self(11);
}

/** Newtype for Glk wintype */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct wintype(pub u32);
impl wintype {
    /** Not an actual window type, but special value for some API functions. */
    pub const AllTypes: Self = Self(0);
    pub const Pair: Self = Self(1);
    pub const Blank: Self = Self(2);
    pub const TextBuffer: Self = Self(3);
    pub const TextGrid: Self = Self(4);
    pub const Graphics: Self = Self(5);
}

/** Newtype for Glk winmethod */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct winmethod(pub u32);
impl winmethod {
    /** The new window will be to the left of the old one which was split. */
    pub const Left: Self = Self(0x00);
    /** The new window will be to the right of the old one which was split. */
    pub const Right: Self = Self(0x01);
    /** The new window will be above the old one which was split. */
    pub const Above: Self = Self(0x02);
    /** The new window will be below the old one which was split. */
    pub const Below: Self = Self(0x03);
    /** Mask covering part of the bit field that assigns a direction. */
    pub const DirMask: Self = Self(0x0f);

    /** The new window is a fixed size. */
    pub const Fixed: Self = Self(0x10);
    /** The new window is a given proportion of the old window's size. */
    pub const Proportional: Self = Self(0x20);
    /** Mask covering part of the bit field that assigns the window division. */
    pub const DivisionMask: Self = Self(0xf0);

    /** There should be a visible window border between the new window and its sibling. */
    pub const Border: Self = Self(0x000);
    /** There should not be a visible window border between the new window and its sibling. */
    pub const NoBorder: Self = Self(0x100);
    /** Mask covering part of the bit field that assigns the border type. */
    pub const BorderMask: Self = Self(0x100);

    /** Get direction component of window method. */
    pub fn dir(&self) -> Self {
        return Self(self.0 & Self::DirMask.0);
    }
    /** Get division component of window method. */
    pub fn division(&self) -> Self {
        return Self(self.0 & Self::DivisionMask.0);
    }
    /** Get border component of window method. */
    pub fn border(&self) -> Self {
        return Self(self.0 & Self::BorderMask.0);
    }
}

/** Newtype for Glk fileusage */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct fileusage(pub u32);
impl fileusage {
    /** Any other kind of file (preferences, statistics, arbitrary data.) */
    pub const Data: Self = Self(0x00);
    /** A file which stores game state. */
    pub const SavedGame: Self = Self(0x01);
    /** A file which contains a stream of text from the game (often an echo stream from a window.) */
    pub const Transcript: Self = Self(0x02);
    /** A file which records player input. */
    pub const InputRecord: Self = Self(0x03);
    /** Mask covering part of the bit field that assigns the file type. */
    pub const TypeMask: Self = Self(0x0f);

    /** The file contents will be stored exactly as they are written, and read back in the same
     * way. */
    pub const TextMode: Self = Self(0x100);
    /** The file contents will be transformed to a platform-native text file as they are written
     * out. */
    pub const BinaryMode: Self = Self(0x000);

    /** Return 'type' component of usage. */
    pub fn get_type(&self) -> Self {
        Self(self.0 & Self::TypeMask.0)
    }
    /** Return whether usage is text mode. */
    pub fn is_text(&self) -> bool {
        (self.0 & Self::TextMode.0) != 0
    }
}

/** Newtype for Glk filemode */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct filemode(pub u32);
impl filemode {
    /** An output stream. */
    pub const Write: Self = Self(0x01);
    /** An input stream. */
    pub const Read: Self = Self(0x02);
    /** Both an input and output stream. */
    pub const ReadWrite: Self = Self(0x03);
    /** An output stream, but the data will added to the end of whatever already existed in the
     * destination, instead of replacing it. */
    pub const WriteAppend: Self = Self(0x05);
}

/** Newtype for Glk seekmode */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct seekmode(pub u32);
impl seekmode {
    /** Seek pos characters after the beginning of the file. */
    pub const Start: Self = Self(0);
    /** Seek pos characters after the current position (moving backwards if pos is negative.) */
    pub const Current: Self = Self(1);
    /** pos characters after the end of the file. (pos should always be zero or negative, so that
     * this will move backwards to a position within the file.) */
    pub const End: Self = Self(2);
}

/** Newtype for Glk stylehint */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct stylehint(pub u32);
impl stylehint {
    /** How much to indent lines of text in the given style. */
    pub const Indentation: Self = Self(0);
    /** How much to indent the first line of each paragraph. */
    pub const ParaIndentation: Self = Self(1);
    /** Text justification (one of `stylehint_just`). */
    pub const Justification: Self = Self(2);
    /** How much to increase or decrease the font size (relative). */
    pub const Size: Self = Self(3);
    /** The value of this hint must be 1 for heavy-weight fonts (boldface), 0 for normal weight,
     * and -1 for light-weight fonts. */
    pub const Weight: Self = Self(4);
    /** The value of this hint must be 1 for oblique fonts (italic), or 0 for
     * normal angle. */
    pub const Oblique: Self = Self(5);
    /** The value of this hint must be 1 for proportional-width fonts, or 0 for fixed-width. */
    pub const Proportional: Self = Self(6);
    /** The foreground color of the text (RGB, 8 bit per channel). */
    pub const TextColor: Self = Self(7);
    /** The background color of the text (RGB, 8 bit per channel). */
    pub const BackColor: Self = Self(8);
    /** The value of this hint must be 0 for normal printing (TextColor on BackColor), or 1 for
     * reverse printing (BackColor on TextColor). */
    pub const ReverseColor: Self = Self(9);
    /** Number of hints defined by spec. */
    pub const NUMHINTS: Self = Self(10);
}

/** Newtype for Glk stylehint_just */
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct stylehint_just(pub u32);
impl stylehint_just {
    /** Align left. */
    pub const LeftFlush: Self = Self(0);
    /** Full justification. */
    pub const LeftRight: Self = Self(1);
    /** Centered. */
    pub const Centered: Self = Self(2);
    /** Align right. */
    pub const RightFlush: Self = Self(3);
}
